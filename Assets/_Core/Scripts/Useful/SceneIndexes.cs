﻿namespace _Core
{
    public static class SceneIndexes
    {
        public const int Menu = 0;
        public const int Playmode = 1;
        public const int Constructor = 2;
    }
}