﻿using UnityEngine;

namespace _Core
{
    public static class SpriteUtils
    {
        // Returns GameObject consisting only of Source Object's sprites
        public static GameObject GetGameObjectSpriteModel(GameObject sourceObj)
        {
            GameObject avatarGO = new GameObject
            {
                name = $"{sourceObj.name}_Avatar"
            };
            foreach (SpriteRenderer spriteRenderer in sourceObj.GetComponentsInChildren<SpriteRenderer>())
            {
                GameObject child = new GameObject();
                child.name = spriteRenderer.name;
                SpriteRenderer s = child.AddComponent<SpriteRenderer>();
                s.sprite = spriteRenderer.sprite;
                s.transform.localPosition = spriteRenderer.transform.localPosition;
                s.transform.localScale = spriteRenderer.transform.localScale;
                s.gameObject.transform.SetParent(avatarGO.transform);
            }
            avatarGO.AddComponent<BoxCollider2D>();         // TODO add script that will create collider based on children sprites bounds
            return avatarGO;
        }
    }
}