﻿using System.Collections.Generic;
using _Core.Scripts.LevelBuilding;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace _Core
{
    public static class TilemapUtils
    {
        public static IEnumerable<SavedTile> GetSavedTilesFromTilemap(Tilemap tilemap)
        {
            foreach (Vector3Int pos in tilemap.cellBounds.allPositionsWithin)
            {
                if (!tilemap.HasTile(pos)) continue;
                LevelTile levelTile = tilemap.GetTile<LevelTile>(pos);
                yield return new SavedTile(pos, levelTile);
            }
        }

        public static IEnumerable<T> GetTilesFromTilemap<T>(Tilemap tilemap) where T : TileBase
        {
            foreach (Vector3Int pos in tilemap.cellBounds.allPositionsWithin)
            {
                if (!tilemap.HasTile(pos)) continue;
                yield return tilemap.GetTile<T>(pos);
            }
        }
        
        public static IEnumerable<Vector3Int> GetTilesPositions(Tilemap tilemap)
        {
            foreach (Vector3Int pos in tilemap.cellBounds.allPositionsWithin)
            {
                if (!tilemap.HasTile(pos)) continue;
                yield return pos;
            }
        } 

        public static void ClearTilemap(Tilemap tilemap)
        {
            tilemap.ClearAllTiles();
        }

        public static void SetSavedTilesToTilemap(Tilemap tilemap, SavedTile[] savedTiles)
        {
            foreach (SavedTile savedTile in savedTiles)
            {
                tilemap.SetTile(savedTile.position, savedTile.tile);
            }
        }
    }
}