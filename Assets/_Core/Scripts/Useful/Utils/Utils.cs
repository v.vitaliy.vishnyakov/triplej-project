﻿using UnityEngine;

namespace _Core
{
    public static class Utils
    {
        public static bool CheckCollision(GameObject collisionObject, LayerMask collisionMask)
        {
            int mask = 1 << collisionObject.layer;
            return (collisionMask.value & mask) != 0;
        }

        public static int LayerMaskToLayer(LayerMask layerMask)
        {
            return (int) Mathf.Log(layerMask.value, 2);
        }

        public static bool AreEqual(Vector2 v1, Vector2 v2, float tolerance)
        {
            Vector2 v = v2 - v1;
            return v.x < tolerance && v.y < tolerance;
        }
        
        public static Vector2 Vector2FromAngle(float a)
        {
            a *= Mathf.Deg2Rad;
            return new Vector2(Mathf.Cos(a), Mathf.Sin(a));
        }

        public static Vector2 RotateVector2DAround(float angle, Vector2 directionVector)
        {
            return Quaternion.AngleAxis(angle, Vector3.forward) * directionVector;
        }
        
        public static Vector2[] ProjectileArcPoints(int iterations, float speed, float distance, float gravity, float angle)
        {
            float iterationSize = distance / iterations;

            float radians = angle;

            Vector2[] points = new Vector2[iterations + 1];

            for (int i = 0; i <= iterations; i++)
            {
                float x = iterationSize * i;
                float t = x / (speed * Mathf.Cos(radians));
                float y = -0.5f * gravity * (t * t) + speed * Mathf.Sin(radians) * t;

                Vector2 p = new Vector2(x, y);

                points[i] = p;
            }

            return points;
        }
        
        public static GameObject GetAvatar(GameObject sourceObj)
        {
            GameObject avatarGO = new GameObject();
            avatarGO.name = $"{sourceObj.name}_ConstructorAvatar";
            foreach (SpriteRenderer spriteRenderer in sourceObj.GetComponentsInChildren<SpriteRenderer>())
            {
                GameObject child = new GameObject();
                SpriteRenderer s = child.AddComponent<SpriteRenderer>();
                s.sprite = spriteRenderer.sprite;
                s.transform.localPosition = spriteRenderer.transform.localPosition;
                s.gameObject.transform.SetParent(avatarGO.transform);
            }
            avatarGO.AddComponent<BoxCollider2D>();         // TODO add script that will create collider based on children sprites bounds
            return avatarGO;
        }
        
        public static Vector3 GetMousePosition(Camera camera)
        {
            return camera.ScreenToWorldPoint(Input.mousePosition);
        }
        
        private static Vector2 GetTouchPosition(Touch touch, Camera camera)
        {
            return camera.ScreenToWorldPoint(touch.position);
        }
        
        // Had no idea that I would have to implement this tolerance parameter, but sometimes
        // Unity surprises me what little numbers it can return instead of zero 
        public static bool ArePerpendicular(Vector2 v1, Vector2 v2, float tolerance = 0)
        {
            return Mathf.Abs(Vector2.Dot(v1.normalized, v2.normalized)) <= tolerance;
        }

        
        
        /*
        Method that calculates required jump force to reach certain height.
        public static float CalculateJumpSpeed(float jumpHeight, float gravity)
        {
            return Mathf.Sqrt(2 * jumpHeight * gravity);
        }*/
        /*
        Class I found that fires some object to the given target position under some initial angle 
        public class ProjectileFire : MonoBehaviour {
        
            [SerializeField]
            Transform target;
        
            [SerializeField]
            float initialAngle;
        
            void Start () {
                var rigid = GetComponent<Rigidbody>();
        
                Vector3 p = target.position;
        
                float gravity = Physics.gravity.magnitude;
                // Selected angle in radians
                float angle = initialAngle * Mathf.Deg2Rad;
        
                // Positions of this object and the target on the same plane
                Vector3 planarTarget = new Vector3(p.x, 0, p.z);
                Vector3 planarPostion = new Vector3(transform.position.x, 0, transform.position.z);
        
                // Planar distance between objects
                float distance = Vector3.Distance(planarTarget, planarPostion);
                // Distance along the y axis between objects
                float yOffset = transform.position.y - p.y;
        
                float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));
        
                Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));
        
                // Rotate our velocity to match the direction between the two objects
                float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPostion);
                Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;
        
                // Fire!
                rigid.velocity = finalVelocity;
        
                // Alternative way:
                // rigid.AddForce(finalVelocity * rigid.mass, ForceMode.Impulse);
            }
        }*/
    }
}