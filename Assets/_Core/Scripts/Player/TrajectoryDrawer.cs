﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core.Player
{
    // TODO In case trajectory drawer is left the "prediction" algorithm must be changed to one that can "predict"
    // trajectory below the tracked RigidBody position
    
    [RequireComponent(typeof(LineRenderer))]
    public class TrajectoryDrawer : MonoBehaviour
    {
        [Header("Trajectory parameters")]
        [SerializeField] private bool _drawTrajectory;
        [SerializeField] private bool _hideFirstDot;
        [SerializeField] private int _fullDotsCount;
        [SerializeField] private int _shownPercentage;
        [SerializeField] private GameObject _trajectoryDot;
        
        [Header("Force vector parameters")]
        [SerializeField] private float _forceVectorWidth;
        [SerializeField] private bool _drawForceVector;
        [SerializeField] private float _forceVectorLength;
        [SerializeField] private bool _isForceVectorMirrored;
        [SerializeField] [Range(1, 20)] private float _mirroredPartDivider;
        
        [Header("Components")]
        [SerializeField] private LineRenderer _lineRenderer;
        
        private int _dotsCount;
        private List<GameObject> _dots = new();
        private Rigidbody2D _trackedRb;

        private bool _isInitialized;
        private bool _wasCalledThisFrame;
        private Vector3 _targetPos;
        private Vector3 _end;
        private Vector3 _origin;
        
        private void Awake()
        {
            InitializeInternal();
        }

        private void InitializeInternal()
        {
            _lineRenderer.widthMultiplier = _forceVectorWidth;

            if (!_drawTrajectory) return;
            _dotsCount = (int) (_fullDotsCount * (_shownPercentage / 100f));
            for (int i = 0; i < _dotsCount; i++)
            {
                GameObject dot = Instantiate(_trajectoryDot, transform, true);
                _dots.Add(dot);
            }

            ShowFirstDot(!_hideFirstDot);
        }

        public void Initialize(Rigidbody2D trackedRb)
        {
            _trackedRb = trackedRb;
            _isInitialized = true;
        }
        
        public void Draw(Vector3 forceVector)
        {
            if (!_isInitialized) return;
            if (!_wasCalledThisFrame)
            {
                Enable();
            }
            if (_drawForceVector)
            {
                DrawForceVector(forceVector, _trackedRb);
            }
            if (_drawTrajectory)
            {
                DrawTrajectory(forceVector, _trackedRb.position, _trackedRb);
            }
            _wasCalledThisFrame = true;
        }

        private void LateUpdate()
        {
            if (_wasCalledThisFrame)
            {
                _wasCalledThisFrame = false;    
            }
            else
            {
                Disable();    
            }
        }

        private void DrawTrajectory(Vector3 forceVector, Vector3 startingPoint, Rigidbody2D rb)
        {
            Vector3 velocity = forceVector / rb.mass;
            float flightDuration = 2 * velocity.y / Physics.gravity.y;
            float stepTime = flightDuration / _fullDotsCount;
            for (int i = 0; i < _dotsCount; i++)
            {
                float stepTimePassed = stepTime * i;
                Vector3 movementVector = new Vector3(
                    velocity.x * stepTimePassed,
                    velocity.y * stepTimePassed - 0.5f * Physics2D.gravity.y * stepTimePassed * stepTimePassed,
                    velocity.z * stepTimePassed);
                _dots[i].transform.position = -movementVector + startingPoint;
            }  
        }

        private void DrawForceVector(Vector3 forceVector, Rigidbody2D rb)
        {
            _targetPos = rb.position;
            forceVector = forceVector.normalized * _forceVectorLength;
            _end =  forceVector + _targetPos;
            _origin = !_isForceVectorMirrored ? _targetPos : _targetPos + -1 * forceVector / _mirroredPartDivider;
            _lineRenderer.SetPositions(new []
                {_origin, _end });
        }

        private void Enable()
        {
            gameObject.SetActive(true);
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }

        private void ShowFirstDot(bool value)
        {
            _dots[0].SetActive(value);
        }

        private void OnValidate()
        {
            _lineRenderer.enabled = _drawForceVector;
        }
    }
}