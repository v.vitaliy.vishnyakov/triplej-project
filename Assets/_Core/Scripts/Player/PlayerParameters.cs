﻿using UnityEngine;

namespace _Core.Player
{
    [CreateAssetMenu(fileName = "PlayerControllerParams", menuName = "Parameters/Player Controller Parameters", order = 0)]
    public class PlayerParameters : ScriptableObject
    {
        public LayerMask groundLayer;
        [Header("Jump")]
        public float maxJumpForce;
        public float jumpForceGainSpeed;
        public float jumpBlindZoneDeg;
        public int maxJumpsInRow;
        public float minJumpForce;
        [Header("Dash")]
        public float dashSpeed;
        public float maxDashLength;
        public int maxDashInRow;
        [Header("Stick state")]
        public float noSlippingTimeout;
        public float slippingSpeed;
        public bool slipWithAcceleration;
        public float slipAcceleration;
        [Header("Physics Materials References")]
        public PhysicsMaterial2D zeroFriction;
        public PhysicsMaterial2D slidingFriction;
    }
}