﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace _Core.Player
{
    public class PlayerAnimator: MonoBehaviour
    {
        [Header("Body parts references")]
        [SerializeField] private SpriteRenderer _head;
        [SerializeField] private SpriteRenderer _body;
        [SerializeField] private SpriteRenderer _legs;
        [Header("Parameters")]
        [SerializeField] private float _headOnPressOffset;
        [SerializeField] private float _bodyOnPressOffset;
        [SerializeField] private float _legsSqueezeOffset;
        [SerializeField] private float _squeezeTime;

        private Sequence _squeezeUpAnim;
        private Sequence _squeezeDownAnim;
        
        private void Awake()
        {
            InitializeAnimations();
        }

        private void InitializeAnimations()
        {
            // Squeeze down
            Tweener headAnimation = _head.transform.DOLocalMoveY(- _headOnPressOffset, _squeezeTime);
            Tweener bodyAnimation = _body.transform.DOLocalMoveY(- _bodyOnPressOffset, _squeezeTime);
            _squeezeDownAnim = DOTween.Sequence();
            _squeezeDownAnim.Append(headAnimation);
            _squeezeDownAnim.Join(bodyAnimation);
            _squeezeDownAnim.SetRelative(true);
            _squeezeDownAnim.SetAutoKill(false);
            _squeezeDownAnim.Pause();
            
            // Squeeze up
            _squeezeUpAnim = DOTween.Sequence();
            _squeezeUpAnim.Append(_body.transform.DOLocalMoveY(_bodyOnPressOffset, _squeezeTime));
            _squeezeUpAnim.Join(_legs.transform.DOLocalMoveY(_legsSqueezeOffset, _squeezeTime));
            _squeezeUpAnim.SetRelative(true);
            _squeezeUpAnim.SetAutoKill(false);
            _squeezeUpAnim.Pause();
        }
        
        public void PlaySqueezeDownAnimation()
        {
            _squeezeDownAnim.PlayForward();
        }
        
        public void RewindSqueezeDownAnimation()
        {
            _squeezeDownAnim.PlayBackwards();
        }

        public void PlaySqueezeUpAnimation()
        {
            _squeezeUpAnim.PlayForward();
        }
        
        public void RewindSqueezeUpAnimation()
        {
            _squeezeUpAnim.PlayBackwards();
        }
        
        public void PlayFullSqueezeDownAnimation(float time = 0)
        {
            StartCoroutine(Co_PlayFullSqueezeDownAnimation(time));
        }

        private IEnumerator Co_PlayFullSqueezeDownAnimation(float time)
        {
            _squeezeDownAnim.PlayForward();
            if (time == 0)
            {
                yield return new DOTweenCYInstruction.WaitForCompletion(_squeezeDownAnim);
            }
            else
            {
                yield return new WaitForSeconds(time);
            }
            _squeezeDownAnim.PlayBackwards();
        }
        
        public void PlayFullSqueezeUpAnimation()
        {
            StartCoroutine(Co_PlayFullSqueezeUpAnimation());
        }

        private IEnumerator Co_PlayFullSqueezeUpAnimation()
        {
            _squeezeUpAnim.PlayForward();
            yield return new DOTweenCYInstruction.WaitForCompletion(_squeezeUpAnim);
            _squeezeUpAnim.PlayBackwards();
        }
    }
}