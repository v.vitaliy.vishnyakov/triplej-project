﻿using UnityEngine;

namespace _Core.Player
{
    public class AiringState: PlayerState
    {
        private const float _slopeCheckDistance = 0.5f;
        
        public AiringState(PlayerController playerController, PlayerParameters parameters) : base(playerController, parameters) { }
        
        private int _jumpsMade;
        private float _jumpForce;
        private Vector3 _jumpDirection;
        private Vector2 _lastFrameVelocity;
        
        public override void Update()
        {
            _lastFrameVelocity = _player.Rb.velocity;
        }

        public override void OnEnter()
        {
            _player.Rb.sharedMaterial = _parameters.slidingFriction;
        }

        public override void OnCollisionEnter(Collision2D collision)
        {
            var wallStick = _player.CheckForWallStick(); 
            if (wallStick != 0)
            {
                _player.SetNewState(new StickState(_player, _parameters, wallStick));
            }
            if (_player.IsGrounded())
            {
                bool isGroundedOnSlope = IsGroundedOnSlope();
                if (isGroundedOnSlope)
                {
                    _player.SetNewState(new SlidingState(_player, _parameters, true));
                }
                else if (CheckForSlide())
                {
                    _player.SetNewState(new SlidingState(_player, _parameters, false));
                }
                else
                {
                    _player.Rb.sharedMaterial = _parameters.zeroFriction;
                    _player.Animator.PlayFullSqueezeDownAnimation();
                    _player.SetNewState(new StandingState(_player, _parameters));
                }
            }
        }

        private bool IsGroundedOnSlope()
        {
            var hit = Physics2D.Raycast(_player.Transform.position, Vector2.down, _slopeCheckDistance, _parameters.groundLayer);
            return hit.collider != null && !Utils.ArePerpendicular(hit.normal, Vector2.right, 0.001f);
        }

        private bool CheckForSlide()
        {
            float angle = Vector2.Angle(_lastFrameVelocity, Vector2.right);
            return (angle <= _parameters.jumpBlindZoneDeg  && angle !=0 || angle >= 180 - _parameters.jumpBlindZoneDeg && angle !=0);
        }
        
        public override void OnTouchStart(Vector2 touchPosition)
        {
            _player.Animator.PlaySqueezeDownAnimation();
        }

        public override void OnTouchEnd(Vector2 touchPosition)
        {
            _player.Animator.RewindSqueezeDownAnimation();
        }

        public override void OnSwipe(Swipe swipe)
        {
            Vector2 dashDirection = swipe.endPoint - swipe.startPoint;
            _player.SetNewState(new DashRushState(_player, _parameters, dashDirection));
        }
        
    }
}