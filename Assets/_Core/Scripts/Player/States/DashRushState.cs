﻿using _Core.Scripts.Blocks;
using UnityEngine;

namespace _Core.Player
{
    public class DashRushState: PlayerState
    {
        public DashRushState(PlayerController playerController, PlayerParameters parameters,
            Vector2 dashDirection) : base(playerController, parameters)
        {
            _dashDirection = dashDirection;
        }

        private Vector2 _targetPos;
        private Vector2 _dashDirection;
        
        private float _initialGravityScale;
        private float _dashTime;
        
        public override void OnEnter()
        {
            if (_dashDirection.magnitude <= _parameters.maxDashLength)
            {
                _targetPos = _dashDirection + _player.Rb.position;
            }
            else
            {
                _targetPos = _dashDirection.normalized * _parameters.maxDashLength + _player.Rb.position;
            }
            _initialGravityScale = _player.Rb.gravityScale;
            _player.Rb.gravityScale = 0;
            _player.Rb.velocity = new Vector2(0, 0f);
        }
        
        public override void OnExit()
        {
            _player.Rb.velocity = new Vector2(0, 0f);
            _player.Rb.gravityScale = _initialGravityScale;
        }
        
        public override void FixedUpdate()
        {
            _player.Rb.MovePosition(Vector2.MoveTowards(_player.Rb.position, _targetPos, _parameters.dashSpeed * Time.fixedDeltaTime));
            
            if (_player.Rb.position == _targetPos)
            {
                _player.SetNewState(new AiringState(_player, _parameters));    
            }
        }

        public override void OnCollisionEnter(Collision2D collision)
        {
            int wallStick = _player.CheckForWallStick();
            if (wallStick != 0)
            {
                _player.SetNewState(new StickState(_player, _parameters, wallStick));
            }
            else if (collision.gameObject.GetComponent<GlassBlockLayer>() == null)
            { 
                if (_player.IsGrounded())
                {
                    _player.SetNewState(new StandingState(_player, _parameters));
                }
                _player.SetNewState(new AiringState(_player, _parameters));
            }
        }

        public override void OnJump()
        {
            _player.SetNewState(new AiringState(_player, _parameters));
        }
    }
}