﻿using UnityEngine;

namespace _Core.Player
{
    public class SlidingState: PlayerState
    {
        private const float _velocityTolerance = 0.01f;
        // -- TODO expose this to PlayerParameters in the way, so the setting of sliding distance and time was easy and understandable
        private const float _initialHorizontalVelocity = 5f;

        private bool _isSlopeSliding;
        
        public SlidingState(PlayerController playerController, PlayerParameters parameters, bool isSlopeSliding) : base(
            playerController, parameters)
        {
            _isSlopeSliding = isSlopeSliding;
        }

        public override void OnEnter()
        {
            if (!_isSlopeSliding)
            {
                _player.Rb.velocity = new Vector2(_initialHorizontalVelocity * Mathf.Sign(_player.Rb.velocity.x), 0);
            }
            _player.Animator.PlaySqueezeDownAnimation();
        }

        public override void OnExit()
        {
            _player.Rb.sharedMaterial = _parameters.zeroFriction;
            _player.Animator.RewindSqueezeDownAnimation();
        }

        public override void Update()
        {
            if (_player.Rb.velocity.magnitude < _velocityTolerance)
            {
                _player.Rb.velocity = Vector2.zero;
                _player.SetNewState(new StandingState(_player, _parameters));
            }
        }

        public override void OnJump()
        {
            _player.SetNewState(new AiringState(_player, _parameters));
        }
    }
}