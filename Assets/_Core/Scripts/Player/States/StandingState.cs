﻿using UnityEngine;

namespace _Core.Player
{
    public class StandingState: PlayerState
    {
        public StandingState(PlayerController playerController, PlayerParameters parameters) : base(playerController, parameters) { }

        private float _jumpForce;
        private Vector3 _jumpDirection;

        public override void OnTouchStart(Vector2 touchPosition)
        {
            _player.Animator.PlaySqueezeDownAnimation();
        }

        public override void OnTouchEnd(Vector2 touchPosition)
        {
            _player.Animator.RewindSqueezeDownAnimation();
        }

        public override void OnJump()
        {
            _player.SetNewState(new AiringState(_player, _parameters));
        }
    }
}