﻿using UnityEngine;

namespace _Core.Player
{
    public class StickState : PlayerState
    {
        public StickState(PlayerController playerController, PlayerParameters parameters, int wallSide) : base(
            playerController, parameters)
        {
            _wallSide = wallSide;
        }

        private readonly int _wallSide;
        private float _timer;
        private float _initialGravityScale;
        public override void OnEnter()
        {
            _initialGravityScale = _player.Rb.gravityScale;
            _player.Rb.gravityScale = 0;
            _player.Rb.velocity = new Vector2(0,0);
        }

        public override void OnExit()
        {
            _player.Rb.gravityScale = _initialGravityScale;
            _player.Transform.position += new Vector3(0.2f * -1 * _wallSide, 0);
        }
        
        public override void Update()
        {
            _timer += Time.deltaTime;
            if (_timer < _parameters.noSlippingTimeout) return;
            float speed;
            if (!_parameters.slipWithAcceleration)
            {
                speed = _parameters.slippingSpeed;
            }
            else
            {
                 speed = (_parameters.slippingSpeed +
                                _parameters.slipAcceleration * (_timer - _parameters.noSlippingTimeout));
            }
            _player.Rb.velocity += new Vector2(0, -1 * speed * Time.deltaTime);
            if (_player.CheckForWallStick() == 0)
            {
                _player.SetNewState(new AiringState(_player, _parameters));
            }
            if (_player.IsGrounded())
            {
                _player.SetNewState(new StandingState(_player, _parameters));
            }
        }

        public override void OnJump()
        {
            _player.SetNewState(new AiringState(_player, _parameters));
        }

        public override bool IsValidJumpAngle(Vector2 touchPos)
        {
            Vector3 position = _player.Rb.position;
            bool trueCondition = _wallSide == 1 ? touchPos.x > position.x : touchPos.x < position.x;
            return base.IsValidJumpAngle(touchPos) && trueCondition;
        }

        public override bool IsDirectionInBlindZone(Vector2 direction)
        {
            float angle = Vector2.Angle(direction, Vector2.up);
            return (angle <= _parameters.jumpBlindZoneDeg || angle >= 90 - _parameters.jumpBlindZoneDeg);
        }
    }
}