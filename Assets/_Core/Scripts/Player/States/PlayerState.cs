﻿using UnityEngine;

namespace _Core.Player
{
    public abstract class PlayerState
    {
        protected readonly PlayerController _player;
        protected readonly PlayerParameters _parameters;
        
        protected PlayerState(PlayerController playerController, PlayerParameters parameters)
        {
            _player = playerController;
            _parameters = parameters;
        }

        public virtual void Update() {}
        public virtual void FixedUpdate() {}
        public virtual void OnTouchStart(Vector2 touchPosition) {}
        public virtual void OnTouch(float touchTime, Vector2 touchPosition) {}
        public virtual void OnTouchEnd(Vector2 touchPosition) {}
        public virtual void OnSwipe(Swipe swipe) {}
        public virtual void OnEnter() {}
        public virtual void OnExit() {}
        public virtual void OnJump() {}
        public virtual void OnCollisionEnter(Collision2D collision) {}
        public virtual void OnCollisionStay(Collision2D collision) {}
        public virtual void OnCollisionExit(Collision2D collision) {}

        public virtual bool IsValidJumpAngle(Vector2 touchPos)
        {
            return touchPos.y < _player.Rb.position.y;
        }

        public virtual bool IsDirectionInBlindZone(Vector2 direction)
        {
            float angle = Vector2.Angle(direction, Vector2.right);
            return (angle <= _parameters.jumpBlindZoneDeg || angle >= 180 - _parameters.jumpBlindZoneDeg);
        }
    }
}