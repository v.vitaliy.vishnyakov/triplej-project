﻿using System;
using _Core.Scripts;
using _Core.Scripts.Blocks;
using _Core.Scripts.Blocks.Block_Layers;
using UnityEngine;

namespace _Core.Player
{
    [RequireComponent( typeof(PlayerAnimator))]
    public class PlayerController : MonoBehaviour, IEntity
    {
        public event Action OnDeath;
        
        private const float _zeroVelocityTolerance = 0.001f;

        [Header("Parameters")] 
        [SerializeField] private PlayerParameters _params;
        [Header("Body part references")]
        public SpriteRenderer head;
        public SpriteRenderer body;
        public SpriteRenderer legs;
        [SerializeField] private TrajectoryDrawer _trajectoryDrawer;

        public PlayerAnimator Animator { get; private set; }
        public Rigidbody2D Rb { get; private set; }
        public Transform Transform { get; private set; }
        

        private PlayerState _state;

        public PlayerState State => _state;
        
        private float _touchTime;
        private Vector2 _touchPos;
        
        private int _dashCount;
        
        private int _jumpsCount;
        private float _jumpForce;
        private Vector2 _jumpDirection;

        private void Awake()
        {
            Initialize();
        }
        
        private void Initialize()
        {
            Transform = GetComponent<Transform>();
            Rb = GetComponent<Rigidbody2D>();
            Animator = GetComponent<PlayerAnimator>();

            InputManager.OnSwipe += OnSwipe;
            InputManager.OnTouch += OnTouch;
            InputManager.OnTouchStart += OnTouchStart;
            InputManager.OnTouchEnd += OnTouchEnd;
            InputManager.OnTap += OnTap;
        }

        private void Start()
        {
            if (IsGrounded())
            {
                _state = new StandingState(this, _params);
            }
            else
            {
                _state = new AiringState(this, _params);
            }
            _trajectoryDrawer.Initialize(Rb);
            _jumpForce = _params.minJumpForce;
            _headUpPoint = new Vector2(0, head.size.y * 0.5f);
            Rb.sharedMaterial = _params.zeroFriction;
        }

        private Vector2 _drawVector;

        private void Update()
        {
            _touchPos = InputManager.LastTouchPos;
            CalculateJumpDirectionRelatively(Rb.position, _touchPos);
            _state.Update();
            DrawDebug();
        }

        private void FixedUpdate()
        {
            _state.FixedUpdate();
        }

        private Vector2 _headUpPoint;
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (Physics2D.OverlapCircle(head.transform.TransformPoint(_headUpPoint), 0.1f,
                _params.groundLayer))
            {
                Animator.PlayFullSqueezeUpAnimation();
            }
            _state.OnCollisionEnter(other);
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            _state.OnCollisionStay(other);
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            _state.OnCollisionExit(other);
        }

        public void SetNewState(PlayerState newState)
        {
            if (newState is DashRushState)
            {
                if (_dashCount >= _params.maxDashInRow) return;
                _dashCount++;
                _jumpsCount = 0;
            }
            if (newState is StandingState or SlidingState)
            {
                _dashCount = 0;
                _jumpsCount = 0;
            }
            if (newState is StickState)
            {
                _jumpsCount--;
            }
            _state.OnExit();
            _state = newState;
            _state.OnEnter();
        }

        private void OnSwipe(Swipe swipe)
        {
            _state.OnSwipe(swipe);
        }

        private void OnTouchStart(Vector2 touchPos)
        {
            _state.OnTouchStart(touchPos);
        }

        private void OnTouch(float time, Vector2 touchPos)
        {
            if(_state.IsValidJumpAngle(touchPos))
            {
                _trajectoryDrawer.Draw(_jumpDirection * _jumpForce);
            }
            _jumpForce = CalculateJumpForce(time, _params.jumpForceGainSpeed);
            _state.OnTouch(time, touchPos);
        }
        
        private float CalculateJumpForce(float preparationTime, float gainSpeed)
        {
            float jumpForce = preparationTime * gainSpeed + +_params.minJumpForce;
            if (jumpForce > _params.maxJumpForce)
            {
                jumpForce = _params.maxJumpForce;
            }
            return jumpForce;
        }

        private void CalculateJumpDirectionRelatively(Vector2 relativePoint, Vector2 touchPosition)
        {
            Vector2 dir = touchPosition - relativePoint;
            dir = -dir.normalized;
            if (!_state.IsDirectionInBlindZone(dir))
            {
                _jumpDirection = dir;
            }
        }
        
        private void OnTouchEnd(Vector2 touchPos)
        {
            if (IsAbleToJump())
            {
                Jump(_jumpDirection * _jumpForce);
            }
            _state.OnTouchEnd(_touchPos);
            _jumpForce = _params.minJumpForce;
        }

        private bool IsAbleToJump()
        {
            return _jumpsCount < _params.maxJumpsInRow &&
                   Rb.velocity.y <= _zeroVelocityTolerance && 
                   _state.IsValidJumpAngle(_touchPos);
        }
        
        private void Jump(Vector2 jumpVector)
        {
            _state.OnJump();
            Rb.velocity = new Vector2(0, 0);
            Rb.AddForce(jumpVector, ForceMode2D.Impulse);
            _jumpsCount++;
        }

        private void OnTap(Vector2 tapPosition)
        {
            if (!IsAbleToJump()) return;
            if(_state.IsValidJumpAngle(tapPosition))
            {
                _trajectoryDrawer.Draw(_jumpDirection * _jumpForce);
            }
            Jump(_jumpDirection * _params.minJumpForce);
            Animator.PlayFullSqueezeDownAnimation(0.05f);
        }
        
        public bool IsGrounded()
        {
            return Physics2D.OverlapBox(Transform.position, new Vector2(legs.size.x, 0.01f), 0, _params.groundLayer) &&
                   Rb.velocity.y < _zeroVelocityTolerance;
        }

        /// <summary>
        /// Returns:
        /// 0 - no wall stick, 
        /// 1 - wall stick on right side, 
        /// -1 - wall stick on left side.
        /// </summary>
        /// <returns></returns>
        public int CheckForWallStick()
        {
            float xOffset = head.size.x * 0.5f;
            Vector2 checkPoint = head.transform.position;
            Collider2D overlapCollider = Physics2D.OverlapCircle(new Vector2(checkPoint.x + xOffset, checkPoint.y), 0.1f,
                _params.groundLayer);
            if (overlapCollider != null)
            {
                return IsColliderSuitableForWallstick(overlapCollider) ? 1 : 0;
            }
            overlapCollider = Physics2D.OverlapCircle(new Vector2(checkPoint.x - xOffset, checkPoint.y), 0.1f,
                _params.groundLayer);
            if (overlapCollider != null)
            {
                return IsColliderSuitableForWallstick(overlapCollider) ? -1 : 0;
            }
            return 0;
        }

        private bool IsColliderSuitableForWallstick(Collider2D collider2d)
        {
            BlockLayer blockLayer = collider2d.GetComponent<BlockLayer>();
            if (_state is DashRushState && blockLayer is GlassBlockLayer) return false;
            return blockLayer is not (IceBlockLayer or TightBlockLayer);
        }

        public void SetKinematic(bool value)
        {
            if(value) Rb.velocity = Vector2.zero;
            Rb.isKinematic = value;
            Rb.useFullKinematicContacts = value;
        }
        
        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void UnsubscribeFromEvents()
        {
            InputManager.OnSwipe -= OnSwipe;
            InputManager.OnTouch -= OnTouch;
            InputManager.OnTouchStart -= OnTouchStart;
            InputManager.OnTouchEnd -= OnTouchEnd;
            InputManager.OnTap -= OnTap;
        }
        
        private void OnGUI()
        {
            string info = $"Is grounded {IsGrounded()}\n" +
                          $"State {_state}\n" +
                          $"Is kinematic {Rb.isKinematic}\n" +
                          $"Dash count {_dashCount}\n" + 
                          $"Jump count {_jumpsCount}\n";
            GUI.Box(new Rect(0, 0, 200, 200), info, new GUIStyle {fontSize = 50});
        }

        private void DrawDebug() {}
        
        public void Die()
        {
            OnDeath?.Invoke();
        }
    }
}