﻿using System;
using System.Collections.Generic;
using System.Linq;
using _Core.Scripts.Blocks;
using _Core.Scripts.LevelBuilding;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Core.Scripts.Constructor
{
    // !!! WARNING: CHANGING THE ORDER OF TILEGROUPS AND TILES WITHIN GROUP IS STRICTLY PROHIBITED AT RUN-TIME !!!
    // (see TilesSelectingMenu.cs for details)
    
    public class ConstructorManager : Singleton<ConstructorManager>
    {
        [Header("Parameters")]
        [SerializeField] private int _maxLevelWidth;
        [SerializeField] private int _maxLevelHeight;
        [SerializeField] private int _maxUndoActions;
        [SerializeField] private List<TileGroup> _tileGroups;
        [Header("UI References")]
        [SerializeField] private ToggleGroup _toolsToggleGroup;
        [SerializeField] private ToolToggle _brushToolToggle;
        [SerializeField] private ToolToggle _eraserToolToggle;
        [SerializeField] private ToolToggle _handToolToggle;
        [SerializeField] private Button _deleteAllButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Button _undoButton;
        [SerializeField] private TilesSelectingMenu tilesSelectingMenu;
        [Header("Object References")]
        [SerializeField] private Camera _camera;
        [Header("Component references")]
        [SerializeField] private EventSystem _eventSystem;
        [SerializeField] private GraphicRaycaster _raycaster;
        [Header("Blocks Params")]
        [SerializeField] private ConstructorBlockLayer _constructorBlockLayerPrefab;
        [SerializeField] private Transform _blockLayersParent;
        [Header("Unique object  references")]
        [SerializeField] private GameObject _playerObjectReference;
        [SerializeField] private GameObject _winPlatformObjectReference;
        [Header("Hand Tool params")]
        [SerializeField] private LayerMask _constraintObjectsLayer;
        
        [Header("Level Saving Metadata [DEVELOPMENT]")]
        [SerializeField] private string _levelTitle;
        [SerializeField] private string _levelAuthor;

        // Hand Tool stuff
        private GameObject _grabbedObject;
        private GameObject _playerObject;
        private GameObject _winPlatformObject;
        
        // Blocks stuff
        private ConstructorBlockLayer _currentBlockLayer;
        private Dictionary<BlockType, ConstructorBlockLayer> _blockLayers = new();
        
        private LevelTile _selectedTile;
        private List<Tool> _usedTools = new();
        private ToolEnum _selectedToolEnum;
        
        private Vector2 _touchStart;
        
        protected override void Initialize()
        {
            _brushToolToggle.Initialize(_toolsToggleGroup, OnToolChanged);
            _eraserToolToggle.Initialize(_toolsToggleGroup, OnToolChanged);
            _handToolToggle.Initialize(_toolsToggleGroup, OnToolChanged);
            SubscribeToEvents();
        }
        
        private void Start()
        {
            InitializeBlockLayers();
            CreateUniqueObjectsFromReferences();
            tilesSelectingMenu.Initialize(_tileGroups);
            LoadLevel(); 
        }

        private void InitializeBlockLayers()
        {
            // Iterating through BlockType enum
            foreach (BlockType block in Enum.GetValues(typeof(BlockType)))
            {
                ConstructorBlockLayer blockLayer = Instantiate(_constructorBlockLayerPrefab, _blockLayersParent); 
                blockLayer.name = $"{block}_BlockLayer";
                if (block == BlockType.Tight) blockLayer.SetSortingOrder(1);
                _blockLayers.Add(block, blockLayer);
            }
        }

        private void CreateUniqueObjectsFromReferences()
        {
            string signature = "_UniqueObject";
            _playerObject = SpriteUtils.GetGameObjectSpriteModel(_playerObjectReference);
            _playerObject.name = "Player" + signature;
            _playerObject.layer = Utils.LayerMaskToLayer(_constraintObjectsLayer);
            _winPlatformObject = SpriteUtils.GetGameObjectSpriteModel(_winPlatformObjectReference);
            _winPlatformObject.name = "WinPlatform" + signature;
            _winPlatformObject.layer = Utils.LayerMaskToLayer(_constraintObjectsLayer);
        }

        [ContextMenu("Save level")]
        private void SaveLevel()
        {
            LevelMetadata levelMetadata = new LevelMetadata
            {
                title = _levelTitle,
                author = _levelAuthor
            };
            LevelData levelData = new LevelData
            {
                playerSpawnPosition = _playerObject.transform.position,
                winPlatformPosition = _winPlatformObject.transform.position
            };
            LevelBuilder.SaveConstructedLevel(_blockLayers.Values.ToArray(), levelMetadata, levelData);
        }

        [ContextMenu("Load level")]
        private void LoadLevel()
        {
            LevelMetadata levelMetadata = new LevelMetadata
            {
                title = _levelTitle,
                author = _levelAuthor,
            };
            SavedLevel level = LevelBuilder.GetSavedLevel(levelMetadata);
            foreach (SavedTile savedTile in level.data.tiles)
            {
                _blockLayers[savedTile.tile.Type].SetTile(savedTile.tile, savedTile.position);
            }
            _playerObject.transform.position = level.data.playerSpawnPosition;
            _winPlatformObject.transform.position = level.data.winPlatformPosition;
            _camera.transform.position = new Vector3(level.data.playerSpawnPosition.x, level.data.playerSpawnPosition.y, -2f);
        }

        private void OnToolChanged(ToolEnum newSelectedToolEnum)
        {
            _selectedToolEnum = newSelectedToolEnum;
        }

        private void OnDeleteAllButtonClick()
        {
            DeleteAllTool deleteAllTool = new DeleteAllTool(_blockLayers.Values.ToArray());
            deleteAllTool.Do();
            AddUsedTool(deleteAllTool);
        }

        private void AddUsedTool(Tool tool)
        {
            if (_usedTools.Count >= _maxUndoActions)
            {
                _usedTools.RemoveAt(0);    
            }
            if (_usedTools.Count == 0)
            {
                _undoButton.interactable = true;
            }
            _usedTools.Add(tool);
        }

        private void OnExitButtonClick()
        {
            SaveLevel();
            foreach (ConstructorBlockLayer blockLayer in _blockLayers.Values)
            {
                blockLayer.Clear();
            }
            SceneManager.LoadScene(SceneIndexes.Menu, LoadSceneMode.Single);
        }

        private void OnUndoButtonClick()
        {
            UndoLastToolUsage();
        }

        private void UndoLastToolUsage()
        {
            if (_usedTools.Count == 0) return;
            _usedTools[^1].Undo();
            _usedTools.RemoveAt(_usedTools.Count-1);
            if (_usedTools.Count == 0)
            {
                _undoButton.interactable = false;
            }
        }
        
        private void OnTouchStart(Vector2 touchPos)
        {
            if (_selectedToolEnum == ToolEnum.Hand)
            {
                RaycastHit2D hit = Physics2D.Raycast(touchPos,
                    Vector2.zero, 100, _constraintObjectsLayer);
                if (hit)
                {
                    _grabbedObject = hit.transform.gameObject;
                }
                else
                {
                    _touchStart = touchPos;
                }
            }
        }
        
        private void OnTouch(float time, Vector2 touchPos)
        {
            if (Input.touchCount > 1 || IsTouchOverUI(Input.mousePosition)) return;
            if (_selectedToolEnum == ToolEnum.Hand)
            {
                if (_grabbedObject != null)
                {
                    _grabbedObject.transform.position = touchPos;
                }
                else
                {
                    Vector3 direction = _touchStart - touchPos;
                    _camera.transform.position += direction;
                }
            }
            else
            {
                HandleDrawingTool(touchPos);
            }

        }

        private void OnTouchEnd(Vector2 touchPos)
        {
            if (_selectedToolEnum == ToolEnum.Hand)
            {
                _grabbedObject = null;
            }
        }
        
        private void OnTap(Vector2 tapPosition)
        {
            if (Input.touchCount > 1) return;
            if (IsTouchOverUI(Input.mousePosition)) return;
            HandleDrawingTool(tapPosition);
        }

        private bool CanDraw(Vector2 position)
        {
            return !(position.x > _maxLevelWidth || position.x < 0 || position.y > _maxLevelHeight ||
                position.y < 0);
        }
        
        private void HandleDrawingTool(Vector2 touchPos)
        {
            if (!CanDraw(touchPos)) return;
            Vector3Int cellPosition = new Vector3Int((int)touchPos.x, (int)touchPos.y);
            if (_selectedToolEnum == ToolEnum.Brush 
                && !IsConstantBlockAtPosition(cellPosition) 
                && !IsBlockAtPositionOfType(_selectedTile.Type, cellPosition))
            {
                DrawTool drawTool = new DrawTool(_currentBlockLayer, _selectedTile, cellPosition);
                drawTool.Do();
                AddUsedTool(drawTool);
            }
            else if (_selectedToolEnum == ToolEnum.Eraser 
                     && IsAnyBlockAtPosition(cellPosition) 
                     && !IsConstantBlockAtPosition(cellPosition))
            {
                EraseTool eraseTool = new EraseTool(_blockLayers.Values.ToArray(), cellPosition);
                eraseTool.Do();
                AddUsedTool(eraseTool);
            }
        }

        private bool IsAnyBlockAtPosition(Vector3Int position)
        {
            foreach (ConstructorBlockLayer blockLayer in _blockLayers.Values)
            {
                if (blockLayer.GetTile(position) != null) return true;
            }
            return false;
        }

        private bool IsConstantBlockAtPosition(Vector3Int position)
        {
            foreach (ConstructorBlockLayer blockLayer in _blockLayers.Values)
            {
                LevelTile tile = blockLayer.GetTile(position);
                if (tile != null && tile.IsConstant) return true;
            }
            return false;
        }

        private bool IsBlockAtPositionOfType(BlockType blockType, Vector3Int position)
        {
            foreach (ConstructorBlockLayer blockLayer in _blockLayers.Values)
            {
                LevelTile tile = blockLayer.GetTile(position);
                if (tile!= null && tile.Type == blockType) return true;
            }
            return false;
        }
        
        
        private bool IsTouchOverUI(Vector2 touchPos)
        {
            PointerEventData pointerEventData = new PointerEventData(_eventSystem)
            {
                position = touchPos
            };
            List<RaycastResult> results = new List<RaycastResult>();
            _raycaster.Raycast(pointerEventData, results);
            return results.Count > 0;
        }

        public static void SetBrushTile(int tilesGroupIndex, int tileIndex)
        {
            _instance._selectedTile = _instance._tileGroups[tilesGroupIndex].GetTileByIndex(tileIndex);
            _instance._brushToolToggle.SetIcon(_instance._selectedTile.ConstructorSprite, _instance._selectedTile.color);
            _instance._currentBlockLayer = _instance._blockLayers[_instance._selectedTile.Type];
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }
        
        private void SubscribeToEvents()
        {
            _deleteAllButton.onClick.AddListener(OnDeleteAllButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
            _undoButton.onClick.AddListener(OnUndoButtonClick);
            InputManager.OnTouch += OnTouch;
            InputManager.OnTap += OnTap;
            InputManager.OnTouchStart += OnTouchStart;
            InputManager.OnTouchEnd += OnTouchEnd;
        }

        private void UnsubscribeFromEvents()
        {
            _deleteAllButton.onClick.RemoveListener(OnDeleteAllButtonClick);
            _exitButton.onClick.RemoveListener(OnExitButtonClick);
            _undoButton.onClick.RemoveListener(OnUndoButtonClick);
            InputManager.OnTouch -= OnTouch;
            InputManager.OnTap -= OnTap;
            InputManager.OnTouchStart -= OnTouchStart;
            InputManager.OnTouchEnd -= OnTouchEnd;
        }

#if UNITY_EDITOR
        private void OnGUI()
        {
            string info = $"Used tools num: {_usedTools.Count}";
            GUI.Box(new Rect(0,0,200,200), info, new GUIStyle() {fontSize = 50});
        }
#endif
        
        #region EditorScripts
#if UNITY_EDITOR
        [ContextMenu("Create Default Level Template From This Level")]
        private void CreateDefaultLevelTemplateFromThisLevel()
        {
            LevelMetadata levelMetadata = new LevelMetadata
            {
                author = _levelAuthor,
                title = _levelTitle
            };
            LevelData levelData = new LevelData
            {
                playerSpawnPosition = _playerObject.transform.position,
                winPlatformPosition = _winPlatformObject.transform.position
            };
            LevelBuilder.CreateDefaultLevelTemplate(_blockLayers.Values.ToArray(), levelMetadata, levelData);
        }
#endif
        #endregion
    }
}



