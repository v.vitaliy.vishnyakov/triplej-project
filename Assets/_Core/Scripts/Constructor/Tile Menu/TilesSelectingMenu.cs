﻿using System.Collections.Generic;
using _Core.Scripts.LevelBuilding;
using UnityEngine;
using UnityEngine.UI;

namespace _Core.Scripts.Constructor
{
    // This menu fully relies on the TileGroups order in ConstructorManager. TileGroups list as well as
    // TileViews rely on the order of LevelTiles within TileGroup. So in future it maybe would be considered reasonable
    // to remove this dependency.
    // First Tile of TileGroup is shown at TileGroupView image.
    
    // !!! WARNING: CHANGING THE ORDER OF TILEGROUPS AND TILES WITHIN GROUP IS STRICTLY PROHIBITED AT RUN-TIME !!!
    
    public class TilesSelectingMenu : MonoBehaviour
    {
        [Header("Objects")]
        [SerializeField] private TilesWindow initialTilesWindow;
        [SerializeField] private Button _backgroundButton;
        [SerializeField] private Transform _tileGroupLayoutTransform;
        [SerializeField] private ToggleGroup _tileViewsToggleGroup;
        [SerializeField] private ToggleGroup _tilesGroupViewToggleGroup;
        [Header("Prefabs")]
        [SerializeField] private TileView _tileViewPrefab;
        [SerializeField] private TileGroupView _tileGroupViewPrefab;
        [SerializeField] private TilesWindow tilesWindowPrefab;
        [SerializeField] private Transform _tilesScreensParent;
        
        private int _currentTilesGroupIndex;
        private readonly List<TileView> _allTileViews = new();
        private readonly List<TileGroupView> _tileGroupViews = new();
        private readonly List<TilesWindow> _tilesScreens = new();

        private bool IsShown => _tilesScreensParent.gameObject.activeSelf;
        
        public void Initialize(List<TileGroup> tileGroups)
        {
            _backgroundButton.onClick.AddListener(OnBackgroundButtonClickHandler);
            
            // Getting TilesScreen position and destroying it
            Vector3 tileScreensPosition = initialTilesWindow.transform.position;
            Destroy(initialTilesWindow.gameObject);
            
            for (int i = 0; i < tileGroups.Count; i++)
            {
                TileGroup tileGroup = tileGroups[i];
                // Creating TileViews for each Tile within this TilesGroup
                List<TileView> tileViews = new List<TileView>();
                for (int j = 0; j < tileGroup.tiles.Count; j++)
                {
                    LevelTile tile = tileGroup.tiles[j];
                    TileView tileView = Instantiate(_tileViewPrefab);
                    tileView.name = tile.name + " View";
                    tileView.Initialize(j, tile.ConstructorSprite, tile.color, _tileViewsToggleGroup);
                    tileView.OnTileSelected += OnTileSelectedHandler;
                    tileViews.Add(tileView);
                    _allTileViews.Add(tileView);
                }

                // Initializing TileGroupView for this TilesGroup
                TileGroupView tileGroupView = Instantiate(_tileGroupViewPrefab, _tileGroupLayoutTransform);
                tileGroupView.name = tileGroup.name + " View";
                LevelTile firstTileInGroup = tileGroup.GetTileByIndex(0);
                tileGroupView.Initialize(i, firstTileInGroup.sprite,firstTileInGroup.color, _tilesGroupViewToggleGroup);
                tileGroupView.OnNewTilesGroupSelected += OnNewTileGroupSelectedHandler;
                tileGroupView.OnClick += OnTileGroupViewClickHandler;
                _tileGroupViews.Add(tileGroupView);
                
                // Initializing TilesScreen for this TilesGroup
                TilesWindow tilesWindow = Instantiate(tilesWindowPrefab, _tilesScreensParent, true);
                tilesWindow.transform.position = tileScreensPosition;
                tilesWindow.name = tileGroup.name + " Screen";
                tilesWindow.Initialize(tileViews);
                _tilesScreens.Add(tilesWindow);
            }

            _tilesGroupViewToggleGroup.SetAllTogglesOff();
            _tileViewsToggleGroup.SetAllTogglesOff();

            _allTileViews[0].SetOn();

            HideTilesWindow();
        }

        private void OnTileSelectedHandler(int selectedTileIndex)
        {
            ConstructorManager.SetBrushTile(_currentTilesGroupIndex, selectedTileIndex);
            HideTilesWindow();
        }
        
        private void OnNewTileGroupSelectedHandler(int selectedTileGroupIndex)
        {
            _currentTilesGroupIndex = selectedTileGroupIndex;
            _tilesScreens[_currentTilesGroupIndex].transform.SetAsLastSibling();
        }

        private void OnTileGroupViewClickHandler()
        {
            ShowTilesWindow();
        }

        private void OnBackgroundButtonClickHandler()
        {
            HideTilesWindow();
        }

        private void ShowTilesWindow()
        {
            if (IsShown) return;
            _backgroundButton.gameObject.SetActive(true);
            _tilesScreensParent.gameObject.SetActive(true);
        }
        
        private void HideTilesWindow()
        {
            if (!IsShown) return;
            _backgroundButton.gameObject.SetActive(false);
            _tilesScreensParent.gameObject.SetActive(false);
            _currentTilesGroupIndex = -1;
        }
        
        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void UnsubscribeFromEvents()
        {
            foreach (TileGroupView tileGroupView in _tileGroupViews)
            {
                tileGroupView.OnNewTilesGroupSelected -= OnNewTileGroupSelectedHandler;
                tileGroupView.OnClick -= OnTileGroupViewClickHandler;
            }

            foreach (TileView tileView in _allTileViews)
            {
                tileView.OnTileSelected -= OnTileSelectedHandler;
            }

            _backgroundButton.onClick.RemoveListener(OnBackgroundButtonClickHandler);
        }
    }
}