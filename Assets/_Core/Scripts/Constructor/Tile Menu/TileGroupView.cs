﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace _Core.Scripts.Constructor
{
    public class TileGroupView : MonoBehaviour, ISelectHandler
    {
        public event Action OnClick;
        public event Action<int> OnNewTilesGroupSelected;
        
        [SerializeField] private Toggle _toggle;
        [SerializeField] private Image _image;
        
        private int _index;
        
        private void Awake()
        {
            _toggle.onValueChanged.AddListener(OnToggleValueChangedHandler);
        }

        public void Initialize(int index, Sprite spriteImage, Color spriteColor, ToggleGroup toggleGroup)
        {
            _index = index;
            _image.sprite = spriteImage;
            _image.color = spriteColor;
            _toggle.group = toggleGroup;
        }
        
        private void OnToggleValueChangedHandler(bool value)
        {
            if (value)
            {
                OnNewTilesGroupSelected?.Invoke(_index);
            }
        }

        private void OnDestroy()
        {
            _toggle.onValueChanged.RemoveListener(OnToggleValueChangedHandler);
        }

        public void OnSelect(BaseEventData eventData)
        {
            OnClick?.Invoke();
        }
    }
}