﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Core.Scripts.Constructor
{
    public class TileView : MonoBehaviour
    {
        public event Action<int> OnTileSelected;
        [SerializeField] private Toggle _toggle;
        [SerializeField] private Image _image;

        private int _indexWithinGroup;

        private void Awake()
        {
            _toggle.onValueChanged.AddListener(OnToggleValueChanged);
        }

        public void Initialize(int indexWithinGroup, Sprite imageSprite, Color imageColor, ToggleGroup toggleGroup)
        {
            _indexWithinGroup = indexWithinGroup;
            _image.sprite = imageSprite;
            _image.color = imageColor;
            _toggle.group = toggleGroup;
        }

        public void SetOn()
        {
            _toggle.isOn = true;
        }

        private void OnToggleValueChanged(bool value)
        {
            if (value)
            {
                OnTileSelected?.Invoke(_indexWithinGroup);
            }
        }

        private void OnDestroy()
        {
            _toggle.onValueChanged.RemoveListener(OnToggleValueChanged);
        }
    }
}