﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core.Scripts.Constructor
{
    public class TilesWindow: MonoBehaviour
    {
        [SerializeField] private Transform _layoutTransform;

        public void Initialize(List<TileView> tileViews)
        {
            foreach (TileView tileView in tileViews)
            {
                tileView.transform.SetParent(_layoutTransform.transform);
            }
        }
    }
}