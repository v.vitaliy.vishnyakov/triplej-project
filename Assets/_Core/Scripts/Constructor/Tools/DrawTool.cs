﻿using _Core.Scripts.Blocks;
using _Core.Scripts.LevelBuilding;
using UnityEngine;

namespace _Core.Scripts.Constructor
{
    public class DrawTool: Tool
    {
        private Vector3Int _position;
        private ConstructorBlockLayer _blockLayer;
        private LevelTile _tile;

        public DrawTool(ConstructorBlockLayer constructorBlockLayer, LevelTile tile, Vector3Int position)
        {
            _blockLayer = constructorBlockLayer;
            _tile = tile;
            _position = position;
        }
        
        public override void Do()
        {
            _blockLayer.SetTile(_tile, _position);
        }

        public override void Undo()
        {
            _blockLayer.SetTile(null, _position);
        }
    }
}