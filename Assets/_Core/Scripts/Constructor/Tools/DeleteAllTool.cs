﻿using System.Collections.Generic;
using _Core.Scripts.Blocks;
using _Core.Scripts.LevelBuilding;
using UnityEngine;

namespace _Core.Scripts.Constructor
{
    public class DeleteAllTool: Tool
    {
        private ConstructorBlockLayer[] _blockLayers;
        private List<SavedTile>[] _savedTiles;
        
        public DeleteAllTool(ConstructorBlockLayer[] blockLayers)
        {
            _savedTiles = new List<SavedTile>[blockLayers.Length];
            _blockLayers = blockLayers;
        }
        
        public override void Do()
        {
            for (var i = 0; i < _blockLayers.Length; i++)
            {
                _savedTiles[i] = new List<SavedTile>();
                foreach (Vector3Int position in _blockLayers[i].GetTilesPositions())
                {
                    LevelTile levelTile = _blockLayers[i].GetTile(position);
                    if (levelTile.IsConstant) continue;
                    _savedTiles[i].Add(new SavedTile(position, levelTile));
                    _blockLayers[i].SetTile(null, position );
                }
            }
        }

        public override void Undo()
        {
            for (var i = 0; i < _blockLayers.Length; i++)
            {
                foreach (SavedTile savedTile in _savedTiles[i])
                {
                    _blockLayers[i].SetTile(savedTile.tile, savedTile.position);
                }
            }
        }
    }
}