﻿namespace _Core.Scripts.Constructor
{
    public abstract class Tool
    {
        public abstract void Do();
        public abstract void Undo();
    }
}