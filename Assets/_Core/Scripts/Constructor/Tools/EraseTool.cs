﻿using _Core.Scripts.Blocks;
using _Core.Scripts.LevelBuilding;
using UnityEngine;

namespace _Core.Scripts.Constructor
{
    public class EraseTool: Tool
    {
        private Vector3Int _position;
        private LevelTile[] _tiles;
        private ConstructorBlockLayer[] _blockLayers;

        public EraseTool(ConstructorBlockLayer[] blockLayers, Vector3Int position)
        {
            _blockLayers = blockLayers;
            _tiles = new LevelTile[blockLayers.Length];
            for (var i = 0; i < blockLayers.Length; i++)
            {
                var blockLayer = blockLayers[i];
                _tiles[i] = blockLayer.GetTile(position);
            }
            _position = position;
        }
        
        public override void Do()
        {
            foreach (var blockLayer in _blockLayers)
            {
                blockLayer.SetTile(null, _position);
            }
        }

        public override void Undo()
        {
            for (var i = 0; i < _blockLayers.Length; i++)
            {
                _blockLayers[i].SetTile(_tiles[i], _position);
            }
        }
    }
}