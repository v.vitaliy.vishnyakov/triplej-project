﻿using System.Collections.Generic;
using UnityEngine;

public struct Line
{
    public Vector3 startPos;
    public Vector3 endPos;

    public Line(Vector3 startPos, Vector3 endPos)
    {
        this.startPos = startPos;
        this.endPos = endPos;
    }
}

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class LineGridMesh : MonoBehaviour
{
    [SerializeField] private int _xCellCount;
    [SerializeField] private int _yCellCount;
    [SerializeField] private float _cellWidth;
    [SerializeField] private float _cellHeight;
    [SerializeField] private float _xOffset;
    [SerializeField] private float _yOffset;

    void Start()
    {
        CalculateAndBuildMesh();
    }
    
    private void CalculateAndBuildMesh()
    {
        BuildMesh(CalculateLines());
    }

    private List<Line> CalculateLines()
    {
        float gridHeight = _cellHeight * _yCellCount + (_yCellCount - 1) * _yOffset;
        float gridWidth = _cellWidth * _xCellCount + (_xCellCount - 1) * _xOffset;

        List<Line> lines = new List<Line>();
        //Vertical lines
        float step = 0;
        for (int i = 0; i <= _xCellCount; i++)
        {
            if (_xOffset != 0)
            {
                if (i != _xCellCount)
                {
                    float yStep = 0;
                    for (int j = 0; j < _yCellCount; j++)
                    {
                        lines.Add(new Line(new Vector3(step, yStep), new Vector3(step, yStep + _cellHeight)));
                        lines.Add(new Line(new Vector3(step + _cellWidth, yStep),
                            new Vector3(step + _cellWidth, yStep + _cellHeight)));
                        yStep += _cellHeight + _yOffset;
                    }
                    step += _xOffset;   
                }
            }
            else
            {
                lines.Add(new Line(new Vector3(step, 0), new Vector3(step, gridHeight)));
            }
            step += _cellWidth;
        }
        //Horizontal lines
        step = 0;
        for (int i = 0; i <= _yCellCount; i++)
        {
            if (_yOffset != 0)
            {
                if (i != _yCellCount)
                {
                    float xStep = 0;
                    for (int j = 0; j < _xCellCount; j++)
                    {
                        lines.Add(new Line(new Vector3(xStep, step), new Vector3(xStep + _cellWidth, step)));
                        lines.Add(new Line(new Vector3(xStep, step + _cellHeight),
                            new Vector3(xStep + _cellWidth, step + _cellHeight)));
                        xStep += _cellWidth + _xOffset;
                    }
                    step += _yOffset;   
                }
            }
            else
            {
                lines.Add(new Line(new Vector3(0, step), new Vector3(gridWidth, step)));
            }
            step += _cellHeight;
        }
        return lines;
    }

    private void BuildMesh(List<Line> lines)
    {
        Mesh mesh = new Mesh();
        Vector3[] vertices = LinesToVertices(lines);
        mesh.vertices = vertices;
        mesh.SetIndices(VerticesToIndices(vertices), MeshTopology.Lines, 0);
        MeshFilter filter = GetComponent<MeshFilter>();
        filter.mesh = mesh;
    }

    private Vector3[] LinesToVertices(List<Line> lines)
    {
        List<Vector3> vertices = new List<Vector3>();
        foreach (Line line in lines)
        {
            vertices.Add(line.startPos);
            vertices.Add(line.endPos);
        }
        return vertices.ToArray();
    }

    private int[] VerticesToIndices(Vector3[] vertices)
    {
        List<int> indices = new List<int>();
        for (int i = 0; i < vertices.Length; i++)
        {
            indices.Add(i);
        }
        return indices.ToArray();
    }

}