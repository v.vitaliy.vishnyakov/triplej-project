﻿using UnityEngine;

namespace _Core.Scripts.Constructor
{
    public class ConstructorCameraBehaviour : MonoBehaviour
    {
        [SerializeField] private float _zoomedOrthoSize;

        private bool _isZoomed;
        private float _initialOrthoSize;
        private Vector3 _touchStart;
        private Vector3 _zoomPosition;
        private Camera _camera;
        
        private void Awake()
        {
            _camera = GetComponent<Camera>();
            _isZoomed = false;
            _initialOrthoSize = _camera.orthographicSize;
        }

        private void Update()
        {
            CheckForZoom();
        }

        private void CheckForZoom()
        {
            if (Input.touchCount != 2) return;
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;
            float difference = currentMagnitude - prevMagnitude;
            if (difference > 0 && !_isZoomed)
            {
                Zoom();
            }
            else if (difference < 0 && _isZoomed)
            {
                Unzoom();
            }
        }

        private void Zoom()
        {
            _isZoomed = true;
            _camera.orthographicSize = _zoomedOrthoSize;
        }

        private void Unzoom()
        {
            _isZoomed = false;
            _camera.orthographicSize = _initialOrthoSize;
        }
    }
}