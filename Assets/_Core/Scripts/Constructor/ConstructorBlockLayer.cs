﻿using System.Linq;
using _Core.Scripts.LevelBuilding;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace _Core.Scripts.Blocks
{
    [RequireComponent(typeof(Tilemap), typeof(TilemapRenderer))]
    public class ConstructorBlockLayer : MonoBehaviour
    {
        private Tilemap _tilemap;

        private void Awake()
        {
            _tilemap = GetComponent<Tilemap>();
        }

        public void SetTile(LevelTile tile, Vector3Int position)
        {
            _tilemap.SetTile(position, tile);
        }

        public LevelTile GetTile(Vector3Int position)
        {
            return _tilemap.GetTile<LevelTile>(position);
        }

        public Vector3Int[] GetTilesPositions()
        {
            return TilemapUtils.GetTilesPositions(_tilemap).ToArray();
        }

        public SavedTile[] GetSavedTiles()
        {
            return TilemapUtils.GetSavedTilesFromTilemap(_tilemap).ToArray();
        }

        public void SetSortingOrder(int sortingOrder)
        {
            GetComponent<TilemapRenderer>().sortingOrder = sortingOrder;
        }

        public void Clear()
        {
            foreach (Vector3Int pos in _tilemap.cellBounds.allPositionsWithin)
            {
                if (!_tilemap.HasTile(pos)) continue;
                LevelTile levelTile = _tilemap.GetTile<LevelTile>(pos);
                if (!levelTile.IsConstant)
                {
                    _tilemap.SetTile(pos, null);
                }
            }
        }
        
    }
}