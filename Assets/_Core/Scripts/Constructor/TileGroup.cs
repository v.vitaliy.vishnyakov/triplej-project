﻿using System.Collections.Generic;
using _Core.Scripts.LevelBuilding;
using UnityEngine;

namespace _Core.Scripts.Constructor
{
    [CreateAssetMenu(menuName = "Level creation/Tile Group", order = 0)]
    public class TileGroup : ScriptableObject
    {
        public List<LevelTile> tiles;

        public LevelTile GetTileByIndex(int index)
        {
            return tiles[index];
        }
    }
}