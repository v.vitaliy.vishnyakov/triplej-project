﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace _Core.Scripts.Constructor
{
    public class ToolToggle : MonoBehaviour
    {
        public event Action<ToolEnum> OnPicked;
        
        [Header("Parameters")]
        [SerializeField] private ToolEnum toolEnum;
        [Header("Components")]
        [SerializeField] private Toggle _toggle;
        [SerializeField] private Image _icon;

        private Color _defaultColor;
        
        private void Awake()
        {
            _toggle.onValueChanged.AddListener(OnValueChanged);
        }

        public void Initialize(ToggleGroup toggleGroup, Action<ToolEnum> onPickedCallback)
        {
            _toggle.group = toggleGroup;
            OnPicked += onPickedCallback;
        }

        public void SetIcon(Sprite spriteImage, Color color = default)
        {
            _icon.sprite = spriteImage;
            if (color != default)
            {
                _icon.color = color;
            }
        }

        private void OnValueChanged(bool value)
        {
            if (value)
            {
                OnPicked?.Invoke(toolEnum);
            }
        }
    }
}