﻿using System;
using UnityEngine;

namespace _Core.Scripts.LevelBuilding
{
    [Serializable]
    public class SavedTile
    {
        public Vector3Int position;
        public LevelTile tile;

        public SavedTile(Vector3Int position, LevelTile tile)
        {
            this.position = position;
            this.tile = tile;
        }
    }
}