﻿using _Core.Scripts.Constructor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace _Core.Scripts.LevelBuilding
{
    [CreateAssetMenu(menuName = "Level creation/Level Tile", order = 0)]
    public class LevelTile : Tile
    {
        [SerializeField] private BlockType type;
        [SerializeField] private Sprite constructorSpriteOverride;
        [SerializeField] private bool isConstant;

        public BlockType Type => type;
        public Sprite ConstructorSprite => constructorSpriteOverride != null ? constructorSpriteOverride : sprite;
        public bool IsConstant => isConstant;
    }
}