﻿using _Core.Scripts.Blocks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace _Core.Scripts.LevelBuilding
{
    public class LevelBuilder : Singleton<LevelBuilder>
    {
        protected override void Initialize()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            if (!SaveManager.IsLevelFileExists("Test_Dev"))
            {
                SavedLevel level = Resources.Load<LevelTemplate>("DefaultLevelTemplate").savedLevel;
                string serializedLevel = JsonUtility.ToJson(level);
                SaveManager.SaveLevelFile(serializedLevel, "Test_Dev");
            }
        }
        
        public static SavedLevel LoadAndConstructPlaymodeLevel(Tilemap tilemap, LevelMetadata levelMetadata)
        {
            TilemapUtils.ClearTilemap(tilemap);
            //tilemap.ClearAllTiles();
            string levelFileName = _instance.GetLevelFileName(levelMetadata);
            string serializedLevel = SaveManager.LoadLevelFile(levelFileName);
            SavedLevel level;
            if (serializedLevel == null)
            {
                Debug.Log($"LevelFile {levelFileName} is not found. Loading template");
                return default;
            }
            else
            {
                level = JsonUtility.FromJson<SavedLevel>(serializedLevel);
            }
            foreach (SavedTile levelTile in level.data.tiles)
            {
                tilemap.SetTile(levelTile.position, levelTile.tile);
            }
            return level;
        }
        
        public static SavedLevel LoadAndConstructLevel(Tilemap tilemap, LevelMetadata levelMetadata)
        {
            TilemapUtils.ClearTilemap(tilemap);
            //tilemap.ClearAllTiles();
            string levelFileName = _instance.GetLevelFileName(levelMetadata);
            string serializedLevel = SaveManager.LoadLevelFile(levelFileName);
            SavedLevel level;
            if (serializedLevel == null)
            {
                Debug.Log($"LevelFile {levelFileName} is not found. Loading template");
                return default;
            }
            else
            {
                level = JsonUtility.FromJson<SavedLevel>(serializedLevel);
            }
            foreach (SavedTile levelTile in level.data.tiles)
            {
                tilemap.SetTile(levelTile.position, levelTile.tile);
            }
            return level;
        }

        public static SavedLevel GetSavedLevel(LevelMetadata levelMetadata)
        {
            string levelFileName = _instance.GetLevelFileName(levelMetadata);
            string serializedLevel = SaveManager.LoadLevelFile(levelFileName);
            return JsonUtility.FromJson<SavedLevel>(serializedLevel);
        }
        
        public static void SaveConstructedLevel(ConstructorBlockLayer[] blockLayers, LevelMetadata levelMetadata, LevelData levelData)
        {
            foreach (ConstructorBlockLayer constructorBlockLayer in blockLayers)
            {
                levelData.tiles.AddRange(constructorBlockLayer.GetSavedTiles());
            }
            SavedLevel savedLevel = new SavedLevel
            {
                metadata = levelMetadata,
                data = levelData
            };
            string serializedLevel = JsonUtility.ToJson(savedLevel);
            SaveManager.SaveLevelFile(serializedLevel, _instance.GetLevelFileName(levelMetadata));
        }

#if UNITY_EDITOR
        public static void CreateDefaultLevelTemplate(ConstructorBlockLayer[] blockLayers, LevelMetadata levelMetadata, LevelData levelData)
        {
            foreach (ConstructorBlockLayer constructorBlockLayer in blockLayers)
            {
                levelData.tiles.AddRange(constructorBlockLayer.GetSavedTiles());
            }
            SavedLevel savedLevel = new SavedLevel
            {
                metadata = levelMetadata,
                data = levelData
            };
            SaveLevelFile(savedLevel);
        }
#endif


        private string GetLevelFileName(LevelMetadata levelMetadata)
        {
            return $"{levelMetadata.title}_{levelMetadata.author}";
        }

#if UNITY_EDITOR
        public static void SaveLevelFile(SavedLevel level)
        {
            LevelTemplate levelTemplate = ScriptableObject.CreateInstance<LevelTemplate>();
            levelTemplate.savedLevel = level;
            AssetDatabase.CreateAsset(levelTemplate, "Assets/Resources/DefaultLevelTemplate.asset");
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
#endif
    }
}