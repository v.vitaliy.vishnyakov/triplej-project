﻿using System;

namespace _Core.Scripts.LevelBuilding
{
    [Serializable]
    public struct SavedLevel
    {
        public LevelData data;
        public LevelMetadata metadata;
    }
}