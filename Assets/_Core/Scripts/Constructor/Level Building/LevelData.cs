﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Core.Scripts.LevelBuilding
{
    [Serializable]
    public class LevelData
    {
        public int levelScreensCount;
        public Vector2 levelScreenSize;
        public Vector3 playerSpawnPosition;
        public Vector3 winPlatformPosition;
        public List<SavedTile> tiles = new List<SavedTile>();
    }
}