﻿using System;

namespace _Core.Scripts.LevelBuilding
{
    [Serializable]
    public struct LevelMetadata
    {
        public string title;
        public string author;
    }
}