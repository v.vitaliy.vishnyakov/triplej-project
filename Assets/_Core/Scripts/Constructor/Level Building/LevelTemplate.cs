﻿using UnityEngine;

namespace _Core.Scripts.LevelBuilding
{
    [CreateAssetMenu(menuName = "Level building/Level Template", order = 0)]
    public class LevelTemplate : ScriptableObject
    {
        public SavedLevel savedLevel;
    }
}