﻿using UnityEngine;

namespace _Core
{
    public struct Swipe
    {
        public float time;
        public float magnitude;
        public Vector2 startPoint;
        public Vector2 endPoint;
    }
}