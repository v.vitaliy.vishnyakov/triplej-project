﻿using System;
using UnityEngine;

namespace _Core
{
    public class InputManager: Singleton<InputManager>
    {
        public static event Action<Swipe> OnSwipe;
        public static event Action<float, Vector2> OnTouch;
        public static event Action<Vector2> OnTouchStart;
        public static event Action<Vector2> OnTouchEnd;
        public static event Action<Vector2> OnTap;

        [SerializeField] private Camera _camera;
        [SerializeField] private float _swipeThreshold;
        [SerializeField] private float _swipeMinLength;

        public static Vector2 LastTouchPos => _instance._mousePos;
        
        private bool _isTouchStartInvoked;
        private float _touchTime;
        private Vector2 _touchStartPos;
        private Vector2 _mousePos;
        
        protected override void Initialize() { }
        
        private void Update()
        {
            _mousePos = Utils.GetMousePosition(_camera);
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0))
#else
            if(Input.GetTouch(0).phase == TouchPhase.Began)
#endif
            {
                _touchTime = 0;
                _touchStartPos = _mousePos;
                _isTouchStartInvoked = false;
            }
#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
#else
            if(Input.GetTouch(0).phase == TouchPhase.Stationary || Input.GetTouch(0).phase == TouchPhase.Moved)
#endif
            {
                _touchTime += Time.deltaTime;
                if (_touchTime > _swipeThreshold)
                {
                    if (!_isTouchStartInvoked)
                    {
                        OnTouchStart?.Invoke(_mousePos);
                        _isTouchStartInvoked = true;
                    }
                    OnTouch?.Invoke(_touchTime, _mousePos);
                }
            }
#if UNITY_EDITOR
            if (Input.GetMouseButtonUp(0))
#else
            if(Input.GetTouch(0).phase == TouchPhase.Ended)
#endif
            {
                if (_touchTime <= _swipeThreshold)
                {
                    Vector2 swipeVector = _mousePos - _touchStartPos;
                    if (swipeVector.magnitude >= _swipeMinLength)
                    {
                        Swipe swipe = new Swipe
                        {
                            startPoint = _touchStartPos,
                            endPoint = _mousePos,
                            time = _touchTime,
                            magnitude =  swipeVector.magnitude
                        };
                        OnSwipe?.Invoke(swipe);
                    }
                    else
                    {
                        OnTap?.Invoke(_mousePos);
                    }
                }
                else
                {
                    OnTouchEnd?.Invoke(_mousePos);
                }
            }
        }
    }
}