﻿namespace _Core.Scripts.Constructor
{
    public enum BlockType
    {
        Rust = 0,
        Ice = 1,
        Tight = 2,
        Spike = 3,
        Glass = 4,
        Broken = 5
    }
}