﻿using _Core.Player;
using _Core.Scripts.Blocks.Block_Layers;
using UnityEngine;

namespace _Core.Scripts.Blocks
{
    public class GlassBlockLayer : BlockLayer
    {
        [SerializeField] private BrokenGlassBlock _brokenBlockPrefab;
        
        private void OnCollisionEnter2D(Collision2D other)
        {
            // if (!other.gameObject.TryGetComponent(out PlayerController player)) return;
            // if (player.State is DashRushState)
            // {
            //     GetComponent<PlatformEffector2D>().useOneWay = true;
            //     DestroyBlocks(GetCollidedTilesPositions(other));
            // }
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            // if (other.contactCount > 0)
            // {
                if (!other.gameObject.TryGetComponent(out PlayerController player)) return;
                if (player.State is DashRushState)
                {
                    GetComponent<PlatformEffector2D>().useOneWay = true;
                    DestroyBlocks(GetCollidedTilesPositions(other));
                }
            //}
        }

        private void OnCollisionExit2D(Collision2D other)
        {
            if (!other.gameObject.TryGetComponent(out PlayerController player)) return;
            if (player.State is DashRushState)
            {
                GetComponent<PlatformEffector2D>().useOneWay = false;
            }
        }
        
        private void DestroyBlocks(Vector3Int[] tilesPositions)
        {
            if (tilesPositions == null) return;
            foreach (Vector3Int tilePosition in tilesPositions)
            {
                _tilemap.SetTile(tilePosition, null);
                _tilemapCollider.ProcessTilemapChanges();
                _compositeCollider.GenerateGeometry();
                GameObject brokenGlassBlock = Instantiate(_brokenBlockPrefab).gameObject;
                brokenGlassBlock.transform.position = new Vector3(tilePosition.x + 0.5f, tilePosition.y + 0.5f);
            }
        }
    }
}