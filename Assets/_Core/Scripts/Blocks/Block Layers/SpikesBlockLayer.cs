﻿using UnityEngine;

namespace _Core.Scripts.Blocks
{
    public class SpikesBlockLayer : BlockLayer
    {
        private void OnCollisionEnter2D(Collision2D other)
        {
            if (GetCollidedTilesPositions(other) == null) return;
            if (other.gameObject.TryGetComponent(out IEntity entity))
            {
                entity.Die();
            }
        }
    }
}