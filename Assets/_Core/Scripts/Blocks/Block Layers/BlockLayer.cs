﻿using System.Collections.Generic;
using _Core.Scripts.Blocks.Block_Layers;
using _Core.Scripts.LevelBuilding;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace _Core.Scripts.Blocks
{
    [RequireComponent(typeof(Tilemap),
        typeof(TilemapRenderer),
        typeof(Rigidbody2D))]
    [RequireComponent(typeof(TilemapCollider2D), typeof(CompositeCollider2D))]
    public abstract class BlockLayer : MonoBehaviour
    {
        private const int _maxContactsToDetect = 10;
        private const float _pointPushingOffset = 0.03f;

        protected Tilemap _tilemap;
        protected TilemapCollider2D _tilemapCollider;
        protected Rigidbody2D _rb;
        protected CompositeCollider2D _compositeCollider;

        private ContactPoint2D[] _contacts = new ContactPoint2D[_maxContactsToDetect];

        private void Awake()
        {
            _tilemap = GetComponent<Tilemap>();
            _tilemapCollider = GetComponent<TilemapCollider2D>();
            _rb = GetComponent<Rigidbody2D>();
            _compositeCollider = GetComponent<CompositeCollider2D>();
            _rb.bodyType = RigidbodyType2D.Static;
            _tilemapCollider.usedByComposite = true;
        }

        protected Vector3Int[] GetCollidedTilesPositions(Collision2D collision, bool ignoreOverlappedContacts = false)
        {
            List<Vector3Int> tilesPos = new();
            collision.GetContacts(_contacts);
            Vector3Int prevCellPos = Vector3Int.zero;
            for (var i = 0; i < collision.contactCount; i++)
            {
                ContactPoint2D contact = _contacts[i];
                Vector2 pushedPos = contact.point + contact.normal * _pointPushingOffset;
                Vector3Int cellPos = new Vector3Int((int) pushedPos.x, (int) pushedPos.y);
                
                // this is bad place for this but I haven't thought of anything better for now
                // TODO find another place for this code or change the whole mechanism of cancelling collision actions is specific situations
                if (!ignoreOverlappedContacts)
                {
                    // example of specific situation. incoming collider came from above and TightBlock prevents
                    // any reaction from this block
                    if (BlocksManager.IsTightBlockAtPosition(cellPos))
                    {
                        // unfortunately, Unity not always returns stable contact point, so sometimes it may be detected
                        // inside of incoming collider though it's higher that this one
                        if (Mathf.Abs(contact.point.y - collision.collider.attachedRigidbody.position.y) < 0.01) return null;
                    }    
                }
                
                if (cellPos == prevCellPos) continue;
                tilesPos.Add(cellPos);
                prevCellPos = cellPos;
            }
            
#if UNITY_EDITOR
            if (_printCollidedTilesPositions)
            {
                string info = "";
                for (var i = 0; i < tilesPos.Count; i++)
                {
                    Vector3Int tilePos = tilesPos[i];
                    info += $"{i}. {tilePos}\n";
                }
            }
#endif
            return tilesPos.ToArray();
        }

        public void SetTile(LevelTile tile, Vector3Int position)
        {
            _tilemap.SetTile(position, tile);
        }

        public bool HasBlock(Vector3Int position)
        {
            return _tilemap.HasTile(position);
        }

        #region DEBUG
        [Header("DEBUG")]
        [SerializeField] private bool _printCollidedTilesPositions;
        #endregion
    }
}