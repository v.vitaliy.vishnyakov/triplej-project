﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Core.Scripts.Blocks
{
    public class BrokenBlockLayer : BlockLayer
    {
        [SerializeField] private BrokenBlockParameters _parameters;
        [SerializeField] private ParticleSystem _particlePrefab;
        private class BlockData
        {
            public float standOnTime;
            public int stage = 1;
        }
        private Dictionary<Vector3Int, BlockData> _tileCollisionTime = new();

        private void Start()
        {
            InitializePositionsData();
        }

        private void InitializePositionsData()
        {
            Vector3Int[] tilePositions = TilemapUtils.GetTilesPositions(_tilemap).ToArray();
            foreach (Vector3Int tilePosition in tilePositions)
            {
                _tileCollisionTime.Add(tilePosition, new BlockData());
            }
        }

        private void OnCollisionStay2D(Collision2D other)
        {
            CountCollisionTimeForTiles(GetCollidedTilesPositions(other));
        }

        private void CountCollisionTimeForTiles(Vector3Int[] tilePositions)
        {
            foreach (Vector3Int tilePosition in tilePositions)
            {
                // ContainsKey is used because GetCollidedTilesPosition method is imperfect and can return
                // tilePositions that do not actually posses tiles along with the accurate ones
                if(!_tileCollisionTime.ContainsKey(tilePosition)) continue;
                _tileCollisionTime[tilePosition].standOnTime += Time.deltaTime;
                BlockData data = _tileCollisionTime[tilePosition];
                switch (data.stage)
                {
                    case 1 when data.standOnTime >= _parameters.FirstStageLength:
                        data.stage = 2;
                        data.standOnTime = 0;
                        _tilemap.SetTile(tilePosition, _parameters.SecondStageTile);
                        PlayPiecesParticle(tilePosition);
                        continue;
                    case 2 when data.standOnTime >= _parameters.SecondStageLength:
                        data.stage = 3;
                        data.standOnTime = 0;
                        _tilemap.SetTile(tilePosition, _parameters.ThirdStageTile);
                        PlayPiecesParticle(tilePosition);
                        continue;
                    case 3 when data.standOnTime >= _parameters.ThirdStageLength:
                        _tileCollisionTime.Remove(tilePosition);
                        _tilemap.SetTile(tilePosition, null);
                        _tilemapCollider.ProcessTilemapChanges();
                        PlayPiecesParticle(tilePosition);
                        break;
                }
            }
        }
        
        // This should me remade ad Particle System
        private void PlayPiecesParticle(Vector3Int cellPos)
        {
            Instantiate(_particlePrefab).transform.position = new Vector3(cellPos.x + 0.5f, cellPos.y + 0.5f);
        }
    }
}