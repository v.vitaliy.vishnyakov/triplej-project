﻿using System;
using _Core.Scripts.Blocks.Block_Layers;

namespace _Core.Scripts.Blocks
{
    [Serializable]
    public class BlockLayers
    {
        public RustBlockLayer rustBlockLayer;
        public BrokenBlockLayer brokenBlockLayer;
        public GlassBlockLayer glassBlockLayer;
        public IceBlockLayer iceBlockLayer;
        public SpikesBlockLayer spikesBlockLayer;
        public TightBlockLayer tightBlockLayer;
    }
}