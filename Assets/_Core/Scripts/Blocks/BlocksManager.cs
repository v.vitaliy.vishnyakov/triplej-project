﻿using UnityEngine;

namespace _Core.Scripts.Blocks.Block_Layers
{
    public class BlocksManager : Singleton<BlocksManager>
    {
        [SerializeField] private BlockLayers _blockLayers;
        
        protected override void Initialize() { }

        public static bool IsTightBlockAtPosition(Vector3Int blockPosition)
        {
            return _instance._blockLayers.tightBlockLayer.HasBlock(blockPosition);
        }
    }
}