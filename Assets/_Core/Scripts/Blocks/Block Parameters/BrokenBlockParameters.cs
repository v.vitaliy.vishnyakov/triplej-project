﻿using UnityEngine;
using _Core.Scripts.LevelBuilding;


namespace _Core.Scripts.Blocks
{
    [CreateAssetMenu(menuName = "Block Parameters/Broken Block Parameters", order = 0)]
    public class BrokenBlockParameters : ScriptableObject
    {
       [SerializeField] private LevelTile secondStageTile;
       [SerializeField] private LevelTile thirdStageTile;
       [SerializeField] private float firstStageLength;
       [SerializeField] private float secondStageLength;
       [SerializeField] private float thirdStageLength;
       
       public LevelTile SecondStageTile => secondStageTile;
       public LevelTile ThirdStageTile => thirdStageTile;
       public float FirstStageLength => firstStageLength;
       public float SecondStageLength => secondStageLength;
       public float ThirdStageLength => thirdStageLength;
    }
}