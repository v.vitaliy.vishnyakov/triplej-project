﻿using DG.Tweening;
using UnityEngine;

namespace _Core.Scripts.Blocks.Block_Layers
{
    // This should be remade as Particle System
    [RequireComponent(typeof(Rigidbody2D), typeof(SpriteRenderer))]
    public class BrokenBlockPiece : MonoBehaviour
    {
        [SerializeField] private float _shrinkingTime;
        [SerializeField] private float _rotateAngle;
        
        private void Start()
        {
            transform.DOScale(Vector3.zero, _shrinkingTime).OnComplete(() => Destroy(gameObject));
            transform.DORotate(new Vector3(0, 0, transform.eulerAngles.z + _rotateAngle), _shrinkingTime);
        }
    }
}