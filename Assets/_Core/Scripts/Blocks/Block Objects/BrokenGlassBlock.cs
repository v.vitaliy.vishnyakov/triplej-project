﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace _Core.Scripts.Blocks.Block_Layers
{
    // This should be remade as Particle System
    public class BrokenGlassBlock : MonoBehaviour
    {
        private List<SpriteRenderer> _pieceRenderers;
        private void Awake()
        {
            _pieceRenderers = GetComponentsInChildren<SpriteRenderer>().ToList();
        }

        private void Start()
        {
            foreach (Rigidbody2D rb in GetComponentsInChildren<Rigidbody2D>())
            {
                rb.AddTorque(0.3f, ForceMode2D.Impulse);
                Vector2 forceDir = (Random.insideUnitCircle - Vector2.zero) * Random.Range(1, 2);
                rb.AddForce(forceDir, ForceMode2D.Impulse);
            }
        }

        private void Update()
        {
            for (var index = 0; index < _pieceRenderers.Count; index++)
            {
                SpriteRenderer pieceRenderer = _pieceRenderers[index];
                if (!pieceRenderer.isVisible)
                {
                    Destroy(pieceRenderer.gameObject);
                    _pieceRenderers.RemoveAt(index);
                }
            }
            if (transform.childCount == 0)
            {
                Destroy(gameObject);
            }
        }
    }
}