﻿namespace _Core
{
    public interface ISaveController
    {
        public void Save<T>(string key, T value);
        public T Load<T>(string key) where T : new();
        public void Delete<T>(string key);
    }
}