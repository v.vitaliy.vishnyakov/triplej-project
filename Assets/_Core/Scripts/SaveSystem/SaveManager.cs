﻿using System.IO;
using UnityEngine;

namespace _Core
{
    public class SaveManager : Singleton<SaveManager>
    {
        private const string _levelFilesFolder = "Levels";
        private const string _levelFileExtension = ".jlv";
        
        private readonly ISaveController _saveController = new PlayerPrefsSaveController();

        protected override void Initialize()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void CheckFolders()
        {
            
        }

        public static void Save<T>(string key, T data)
        {
            _instance._saveController.Save(key, data);
        }
        public static T Load<T>(string key) where T : new()
        {
            T smth = _instance._saveController.Load<T>(key);
            return smth;
        }
        public static void Delete<T>(string key)
        {
            _instance._saveController.Delete<T>(key);
        }

        public static void SaveLevelFile(string serializedData, string fileName)
        {
            string path = @$"{Application.persistentDataPath}/{_levelFilesFolder}/{fileName}{_levelFileExtension}";
            string directoryPath = Path.GetDirectoryName(path);
            if (directoryPath == null)
            {
                Debug.LogError("Wrong directory path", _instance);
                return;
            }
            Directory.CreateDirectory(directoryPath);
            File.WriteAllText(path, serializedData);
        }
        
        public static string LoadLevelFile(string fileName)
        {
            string path = @$"{Application.persistentDataPath}/{_levelFilesFolder}/{fileName}{_levelFileExtension}";
            if (File.Exists(path)) return File.ReadAllText(path);
            Debug.LogError("Cant load level file because is does not exist");
            return null;
        }

        public static bool IsLevelFileExists(string fileName)
        {
            string path = @$"{Application.persistentDataPath}/{_levelFilesFolder}/{fileName}{_levelFileExtension}";
            return File.Exists(path);
        }
    }
}