﻿using UnityEngine;

namespace _Core
{
    public class PlayerPrefsSaveController : ISaveController
    {
        public void Save<T>(string key, T value)
        {
            string jsonString = JsonUtility.ToJson(value);
            PlayerPrefs.SetString(key, jsonString);
        }

        public T Load<T>(string key) where T : new()
        {
            string jsonString = PlayerPrefs.GetString(key);
            if (jsonString == string.Empty) return new T();
            return JsonUtility.FromJson<T>(jsonString);
        }

        public void Delete<T>(string key)
        {
            PlayerPrefs.DeleteKey(key);
        }
    }
}