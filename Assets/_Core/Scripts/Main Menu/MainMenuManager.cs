﻿using _Core.Scripts.MainMenu.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Core.Scripts.MainMenu
{
    // For now it's also function as UI manager for this scene
    public class MainMenuManager : Singleton<MainMenuManager>
    {
        [SerializeField] private MainMenuScreen _mainMenuScreen;
        
        protected override void Initialize() { }

        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            _mainMenuScreen.OnPlayButtonClick += OnUserPlaymodeLaunchHandler;
            _mainMenuScreen.OnConstructorButtonClick += OnUserConstructorLaunchHandler;
        }

        private void UnsubscribeFromEvents()
        {
            _mainMenuScreen.OnPlayButtonClick -= OnUserPlaymodeLaunchHandler;
            _mainMenuScreen.OnConstructorButtonClick -= OnUserConstructorLaunchHandler;
        }
        
        private void OnUserPlaymodeLaunchHandler()
        {
            SceneManager.LoadScene(SceneIndexes.Playmode, LoadSceneMode.Single);
        }
        
        private void OnUserConstructorLaunchHandler()
        {
            SceneManager.LoadScene(SceneIndexes.Constructor, LoadSceneMode.Single);
        }
    }
}