﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Screen = _Core.Scripts.UI.Screen;

namespace _Core.Scripts.MainMenu.UI
{
    public class MainMenuScreen : Screen
    {
        public event Action OnPlayButtonClick;
        public event Action OnConstructorButtonClick;
        
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _constructorButton;

        private void Awake()
        {
            _playButton.onClick.AddListener(OnPlayButtonClickHandler);
            _constructorButton.onClick.AddListener(OnConstructorButtonClickHandler);
        }

        private void OnPlayButtonClickHandler()
        {
            OnPlayButtonClick?.Invoke();
        }
        
        private void OnConstructorButtonClickHandler()
        {
            OnConstructorButtonClick?.Invoke();
        }
    }
}