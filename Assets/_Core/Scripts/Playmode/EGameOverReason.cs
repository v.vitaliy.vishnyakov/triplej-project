﻿namespace _Core.Scripts.Playmode
{
    public enum EGameOverReason
    {
        Win,
        Death,
    }
}