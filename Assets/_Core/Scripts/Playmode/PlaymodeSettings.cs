﻿using UnityEngine;

namespace _Core.Scripts.Playmode
{
    [CreateAssetMenu(menuName = "Playmode/Playmode Settings", order = 0)]
    public class PlaymodeSettings : ScriptableObject
    {
        [Header("Animation")] 
        [SerializeField] private float _platformCalibratingTime;

        public float PlatformCalibratingTime => _platformCalibratingTime;
    }
}