﻿using System;
using _Core.Player;
using _Core.Scripts.Blocks;
using _Core.Scripts.Constructor;
using _Core.Scripts.LevelBuilding;
using _Core.Scripts.Playmode.UI;
using _Core.Scripts.Unique_Objects;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Core.Scripts.Playmode
{
    // Possibly there would be other reasons for win/defeat so the enum is used for now

    public class PlaymodeManager : Singleton<PlaymodeManager>
    {
        private event Action<EGameOverReason> OnGameOver;

        [Header("Prefabs")] 
        [SerializeField] private PlaymodeSettings _settings;
        [Header("Object references")] 
        [SerializeField] private PlaymodeCameraBehaviour playmodeCamera;
        [SerializeField] private BlockLayers _blockLayers;
        [Header("Prefabs")]
        [SerializeField] private PlayerController _playerPrefab;
        [SerializeField] private WinPlatform _winPlatformPrefab;
        [Header("Loading level metadata [DEVELOPMENT]")]
        [SerializeField] private string _levelTitle;
        [SerializeField] private string _levelAuthor;
        
        private SavedLevel _level;
        private PlayerController _player;
        private WinPlatform _winPlatform;
        
        protected override void Initialize()
        {
            _player = Instantiate(_playerPrefab);
            playmodeCamera.SetFollowedTransform(_player.Transform);
            _winPlatform = Instantiate(_winPlatformPrefab);
        }
        
        private void Start()
        {
            SubscribeToEvents();
            ConstructLevel();
        }

        private void SubscribeToEvents()
        {
            _winPlatform.OnPlayerContact += OnWinPlatformContact;
            _player.OnDeath += OnPlayerDeath;
            PlaymodeUIManager.OnPlayModeUserExit += OnPlayModeUserExitHandler;
        }

        private void UnsubscribeFromEvents()
        {
            _winPlatform.OnPlayerContact -= OnWinPlatformContact;
            _player.OnDeath -= OnPlayerDeath;
            PlaymodeUIManager.OnPlayModeUserExit -= OnPlayModeUserExitHandler;
        }

        // preferably this should be located in some other place like LevelBuilder (or existing LevelManager) or smth else
        private void ConstructLevel()
        {
            LevelMetadata levelMetadata = new LevelMetadata
            {
                author = _levelAuthor,
                title = _levelTitle
            };
            SavedLevel level = LevelBuilder.GetSavedLevel(levelMetadata);
            foreach (SavedTile savedTile in level.data.tiles)
            {
                switch (savedTile.tile.Type)
                {
                    case BlockType.Rust:
                        _blockLayers.rustBlockLayer.SetTile(savedTile.tile, savedTile.position);
                        break;
                    case BlockType.Ice:
                        _blockLayers.iceBlockLayer.SetTile(savedTile.tile, savedTile.position);
                        break;
                    case BlockType.Tight:
                        _blockLayers.tightBlockLayer.SetTile(savedTile.tile, savedTile.position);
                        break;
                    case BlockType.Spike:
                        _blockLayers.spikesBlockLayer.SetTile(savedTile.tile, savedTile.position);
                        break;
                    case BlockType.Glass:
                        _blockLayers.glassBlockLayer.SetTile(savedTile.tile, savedTile.position);
                        break;
                    case BlockType.Broken:
                        _blockLayers.brokenBlockLayer.SetTile(savedTile.tile, savedTile.position);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            _player.Transform.position = level.data.playerSpawnPosition;
            _winPlatform.transform.position = level.data.winPlatformPosition;
        }

        private void OnWinPlatformContact(Vector3 platformPosition)
        {
            _player.enabled = false;
            _player.Transform.DOMove(platformPosition, _settings.PlatformCalibratingTime).OnComplete(() => FinishGame(EGameOverReason.Win));
        }
        
        private void OnPlayerDeath()
        {
            FinishGame(EGameOverReason.Death);
        }

        private void FinishGame(EGameOverReason eGameOverReason)
        {
            OnGameOver?.Invoke(eGameOverReason);
        }

        private void OnPlayModeUserExitHandler()
        {
            UnsubscribeFromEvents();
            SceneManager.LoadScene(SceneIndexes.Menu, LoadSceneMode.Single);
        }

        public static void SubscribeToGameEvents(GameEventsCallbacks gameEventsCallbacks)
        {
            _instance.OnGameOver += gameEventsCallbacks.OnGameOver;
        }
        
        public static void UnsubscribeFromGameEvents(GameEventsCallbacks gameEventsCallbacks)
        {
            _instance.OnGameOver -= gameEventsCallbacks.OnGameOver;
        }
    }
}