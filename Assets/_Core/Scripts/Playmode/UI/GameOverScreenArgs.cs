﻿using _Core.Scripts.UI;

namespace _Core.Scripts.Playmode.UI
{
    public class GameOverScreenArgs: IScreenShowArgs
    {
        public bool isWin;

        public GameOverScreenArgs(bool isWin)
        {
            this.isWin = isWin;
        }
    }
}