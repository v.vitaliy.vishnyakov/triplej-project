﻿using System;
using _Core.Scripts.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Screen = _Core.Scripts.UI.Screen;

namespace _Core.Scripts.Playmode.UI
{
    public class GameOverScreen : Screen
    {
        public event Action OnExitButtonClick;

        [Header("Parameters")]
        [SerializeField] private string onWinLabel;
        [SerializeField] private string onDefeatLabel;
        [Header("Component references")]
        [SerializeField] private Button _continueButton;
        [SerializeField] private TextMeshProUGUI _resultLabel;
        
        private void Awake()
        {
            _continueButton.onClick.AddListener(OnContinueButtonClick);
        }

        protected override void OnShow(IScreenShowArgs args = null)
        {
            if (args is GameOverScreenArgs gameOverScreenParameters)
            {
                _resultLabel.text = gameOverScreenParameters.isWin ? onWinLabel : onDefeatLabel;
            }
        }

        private void OnContinueButtonClick()
        {
            OnExitButtonClick?.Invoke();
        }
    }
}