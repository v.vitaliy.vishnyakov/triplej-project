﻿using System;
using _Core.Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

namespace _Core.Scripts.Playmode.UI
{
    public class PlaymodeUIManager : UIManager<PlaymodeUIManager>
    {
        public static event Action OnPlayModeUserExit;
        
        [SerializeField] private Button _exitButton;
        
        protected override void Initialize() { }

        private void Start()
        {
            SubscribeToEvents();
        }

        private void OnDestroy()
        {
            UnsubscribeFromEvents();
        }

        private void SubscribeToEvents()
        {
            // Game events
            PlaymodeManager.SubscribeToGameEvents(
                new GameEventsCallbacks
                {
                    OnGameOver = OnGameOverHandler 
                });
            // Screen events
            GetScreen<GameOverScreen>().OnExitButtonClick += OnPlayModeUserExitHandler;
            // Child objects events
            _exitButton.onClick.AddListener(OnPlayModeUserExitHandler);
        }
        
        private void UnsubscribeFromEvents()
        {
            // Game events
            PlaymodeManager.UnsubscribeFromGameEvents(
                new GameEventsCallbacks
                {
                    OnGameOver = OnGameOverHandler 
                });
            // Screen events
            GetScreen<GameOverScreen>().OnExitButtonClick -= OnPlayModeUserExitHandler;
            // Child objects events
            _exitButton.onClick.RemoveListener(OnPlayModeUserExitHandler);
        }

        private void OnGameOverHandler(EGameOverReason eGameOverReason)
        {
            ShowScreen<GameOverScreen>(eGameOverReason == EGameOverReason.Win
                ? new GameOverScreenArgs(true)
                : new GameOverScreenArgs(false));
        }

        private void OnPlayModeUserExitHandler()
        {
            OnPlayModeUserExit?.Invoke();
        }
    }
}