﻿using System;

namespace _Core.Scripts.Playmode
{
    public class GameEventsCallbacks
    {
        public Action<EGameOverReason> OnGameOver;
    }
}