﻿using UnityEngine;

namespace _Core
{
    // TODO probably replace this realization with a different one. Ideas
    // 1. Get rid of the collider usage
    // 2. Use fixed dots for corners so position calculations weren't required, but then provide some editor script
    // so the frame was visible just like the collider now
    // (now collider is used only to easily calculate frame corners positions and to have a visible frame in editor,
    // collisions aren't involved)
    
    [RequireComponent(typeof(BoxCollider2D), typeof(Camera))]
    public class PlaymodeCameraBehaviour : MonoBehaviour
    {
        [SerializeField] private Transform _transformToFollow;

        private Vector2 _leftDownCornerOffset;
        private Vector2 _rightUpCornerOffset;
        private Vector2 _leftDownCorner;
        private Vector2 _rightUpCorner;
        private float _height;
        private float _width;

        private void Awake()
        {
            BoxCollider2D box = GetComponent<BoxCollider2D>();
            _leftDownCornerOffset = new Vector2(box.offset.x - box.size.x * 0.5f, box.offset.y - box.size.y * 0.5f);
            _rightUpCornerOffset = new Vector2(box.offset.x + box.size.x * 0.5f, box.offset.y + box.size.y * 0.5f);
            Camera cameraComponent = GetComponent<Camera>();
            _height = 2 * cameraComponent.orthographicSize;
            _width = _height * cameraComponent.aspect;
        }

        private void Start()
        {
            transform.position = new Vector3(_width * 0.5f, _height * 0.5f, -2f);
        }

        private void LateUpdate()
        {
            Vector2 position = transform.position;
            _leftDownCorner = position + _leftDownCornerOffset;
            _rightUpCorner = position + _rightUpCornerOffset;

            Vector2 translate = new Vector2(0,0);
            if (_transformToFollow.position.x > _rightUpCorner.x)
            {
                translate.x += _transformToFollow.position.x - _rightUpCorner.x;
            }
            if (_transformToFollow.position.x < _leftDownCorner.x)
            {
                translate.x += _transformToFollow.position.x - _leftDownCorner.x;
            }
            if (_transformToFollow.position.y < _leftDownCorner.y)
            {
                translate.y += _transformToFollow.position.y - _leftDownCorner.y;
            }
            if (_transformToFollow.position.y > _rightUpCorner.y)
            {
                translate.y += _transformToFollow.position.y - _rightUpCorner.y;
            }
            transform.Translate(translate);
        }

        public void SetFollowedTransform(Transform transformToFollow)
        {
            _transformToFollow = transformToFollow;
        }
    }
}