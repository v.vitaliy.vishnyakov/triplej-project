﻿namespace _Core.Scripts
{
    public interface IEntity
    {
        public void Die();
    }
}