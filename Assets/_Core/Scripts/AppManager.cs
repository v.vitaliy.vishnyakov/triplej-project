﻿using UnityEngine;

namespace _Core.Scripts
{
    public class AppManager : Singleton<AppManager>
    {
        protected override void Initialize()
        {
            DontDestroyOnLoad(this);
            Application.targetFrameRate = 60;
        }
    }
}