﻿using System;
using UnityEngine;

namespace _Core.Scripts.Unique_Objects
{
    [RequireComponent(typeof(SpriteRenderer), typeof(BoxCollider2D))]
    public class WinPlatform: MonoBehaviour
    {
        public event Action<Vector3> OnPlayerContact;

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (other.collider.CompareTag("Player"))
            {
                OnPlayerContact?.Invoke(GetStandingPosition());
            }
        }

        private Vector3 GetStandingPosition()
        {
            Vector3 pos  = transform.position;
            return new Vector3(pos.x,
                pos.y + GetComponent<SpriteRenderer>().size.y * transform.localScale.y * 0.5f);
        }
    }
}