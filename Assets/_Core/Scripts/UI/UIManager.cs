﻿using System.Collections.Generic;
using UnityEngine;

namespace _Core.Scripts.UI
{
    // This is abstract as I would like to remove some UI logic from ConstructorManager to something like
    // ConstructorUIManager, but it's undone for now
    public abstract class UIManager<T> : Singleton<T> where T: UIManager<T>
    {
        [SerializeField] private List<Screen> _screens;
        
        protected static void ShowScreen<TScreen>(IScreenShowArgs screenShowArgs = null) where TScreen: Screen
        {
            Screen screen = GetScreen<TScreen>();
            screen.Show(screenShowArgs);
        }

        protected static void HideScreen<TScreen>() where TScreen: Screen
        {
            Screen screen =  GetScreen<TScreen>();
            screen.Hide();
        }
        
        protected static TScreen GetScreen<TScreen>() where TScreen : Screen
        {
            foreach (Screen screen in _instance._screens)
            {
                if (screen is TScreen screenT) return screenT;
            }
            return null;
        }
    }
}