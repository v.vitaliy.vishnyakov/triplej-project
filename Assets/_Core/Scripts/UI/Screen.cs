﻿using UnityEngine;

namespace _Core.Scripts.UI
{
    public abstract class Screen : MonoBehaviour
    {
        protected virtual void OnShow(IScreenShowArgs args = null) { }
        
        protected virtual void OnHide() { }
        
        public void Show(IScreenShowArgs args = null)
        {
            OnShow(args);
            gameObject.SetActive(true);
        }
        
        public void Hide()
        {
            OnHide();
            gameObject.SetActive(false);
        }
    }
}